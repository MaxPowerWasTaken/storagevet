"""
Finances.py

This Python class contains methods and attributes vital for completing financial analysis given optimal dispathc.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import logging
import Library as sh
import copy
import re

SATURDAY = 5

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class Financial:

    def __init__(self, params):
        """ Initialized Financial object for case

         Args:
            params (Dict): input parameters
        """

        # assign important financial attributes
        self.tariff = params['customer_tariff']

        self.mpc = params['mpc']
        self.dt = params['dt']
        self.n = params['n']
        self.start_year = params['start_year']
        self.end_year = params['end_year']
        self.opt_years = params['opt_years']
        self.inflation_rate = params['inflation_rate']
        self.npv_discount_rate = params['npv_discount_rate']
        self.growth_rates = {'default': params['def_growth']}
        self.frequency = params['frequency']  # we assume that this is the same as the load data because loaded from the same time_series
        self.verbose = params['verbose']
        # which columns are needed from monthly_data [input validation logic]
        # self.monthly_price_cols = params['monthly_price_cols']
        # self.monthly_prices = params['monthly_prices']

        # create financial inputs data table

        # prep outputs
        self.fin_summary = pd.DataFrame()  # this is just the objective values evaluated at their minimum
        self.pro_forma = pd.DataFrame()
        self.npv = pd.DataFrame()
        self.cost_benefit = pd.DataFrame()
        self.adv_monthly_bill = pd.DataFrame()
        self.sim_monthly_bill = pd.DataFrame()

        # self.federal_tax_rate = params['federal_tax_rate']
        # self.state_tax_rate = params['state_tax_rate']
        # self.property_tax_rate = params['property_tax_rate']

    def calc_energy_bill(self, results, p_energy):
        """ Calculates the retail energy bill for the optimal dispatch by billing period and by month.

        Args:
            results (DataFrame): Dataframe with all the optimization solutions
            p_energy (DataFrame): retail energy price
        """

        results = copy.deepcopy(results)
        results['he'] = (results.index - pd.Timedelta('1s')).hour + 1
        results['yr_mo'] = results.index.to_period('M')
        results['hour fraction'] = results.index.minute / 60
        results['he fraction'] = results['he'] + results['hour fraction']
        results.loc[:, 'net_power'] = results.loc[:, 'dis'] - results.loc[:, 'ch'] - results.loc[:, 'load'] + results.loc[:, 'generation']
        results.loc[:, 'original_net_power'] = - results.loc[:, 'load'] + results.loc[:, 'generation']
        # calculate energy cost every time step
        results.loc[:, 'energy_cost'] = -self.dt * np.multiply(results.loc[:, 'net_power'], p_energy)
        results.loc[:, 'original_energy_cost'] = -self.dt * np.multiply(results.loc[:, 'original_net_power'], p_energy)
        # Calculate Demand Charge per month
        monthly_bill = pd.DataFrame()
        for item in range(len(self.tariff)):
            bill = self.tariff.iloc[item, :]
            month_mask = (bill["Start Month"] <= results.index.month) & (results.index.month <= bill["End Month"])
            time_mask = (bill['Start Time'] <= results['he fraction']) & (results['he fraction'] <= bill['End Time'])
            weekday_mask = True
            exclud_mask = False

            if not bill['Weekday?'] == 2:  # if not (apply to weekends and weekdays)
                weekday_mask = bill['Weekday?'] == (results.index.weekday < SATURDAY).astype('int64')
            if not np.isnan(bill['Excluding Start Time']) and not np.isnan(bill['Excluding End Time']):
                exclud_mask = (bill['Excluding Start Time'] <= results['he fraction']) & (results['he fraction'] <= bill['Excluding End Time'])
            mask = np.array(month_mask & time_mask & np.logical_not(exclud_mask) & weekday_mask)

            temp_df = results[mask]
            demand = -temp_df.groupby(by=['yr_mo'])[['net_power', 'original_net_power']].min() * bill['Value']
            demand.columns = pd.Index(['Demand Charge ($)', 'Original Demand Charge ($)'])

            retail_sub = temp_df.groupby(by=['yr_mo'])[['energy_cost', 'original_energy_cost']].sum()
            retail_sub.columns = pd.Index(['Energy Charge ($)', 'Original Energy Charge ($)'])

            billing_pd = pd.Series(np.repeat(bill.name, len(demand)), name='Billing Period', index=retail_sub.index)

            temp_bill = pd.concat([demand, retail_sub, billing_pd], sort=False, axis=1)
            monthly_bill = monthly_bill.append(temp_bill)
        # monthly_bill.columns = pd.Index(['demand_charge', 'original_demand_charge', 'energy_charge', 'original_energy_charge'])
        monthly_bill = monthly_bill.sort_index(axis=0)
        self.adv_monthly_bill = monthly_bill
        self.adv_monthly_bill.index.name = 'Month-Year'

        sim_demand = monthly_bill[['Demand Charge ($)', 'Original Demand Charge ($)']].groupby(monthly_bill.index.name).sum()
        retail = results.groupby(by=['yr_mo'])[['energy_cost', 'original_energy_cost']].sum()
        retail.columns = pd.Index(['Energy Charge ($)', 'Original Energy Charge ($)'])
        self.sim_monthly_bill = pd.concat([sim_demand, retail], sort=False, axis=1)

        for month_yr_index in monthly_bill.index.unique():
            mo_yr_data = monthly_bill.loc[month_yr_index, :]
            if mo_yr_data.ndim > 1:
                billing_periods = ', '.join(str(int(period)) for period in mo_yr_data['Billing Period'].values)
            else:
                billing_periods = str(int(mo_yr_data['Billing Period']))
            self.sim_monthly_bill.loc[month_yr_index, 'Billing Period'] = '[' + billing_periods + ']'
        self.sim_monthly_bill.index.name = 'Month-Year'

    def yearly_financials(self, technologies, services, results, use_inflation=True):
        """ Aggregates optimization period financials to yearly and interpolated between optimization years

        Args:
            technologies (Dict): Dict of technologies (needed to get capital and om costs)
            services (Dict): Dict of services to calculate cost avoided or profit
            results (DataFrame): DataFrame with all the optimization variable solutions
            use_inflation (bool): Flag to determine if using inflation rate to determine financials for extrapolation. If false, use extrapolation

        """
        # create yearly data table
        yr_index = pd.period_range(start=self.start_year, end=self.end_year, freq='y')
        yr_index = np.insert(yr_index.values, 0, 'CAPEX Year')
        pro_forma = pd.DataFrame(index=yr_index)

        # add capital costs for each technology (only the battery at this time)
        # for tech in technologies.values():
        tech = technologies['Storage']
        ccost_0 = tech.ccost
        ccost_0 += tech.ccost_kw * tech.dis_max_rated
        ccost_0 += tech.ccost_kwh * tech.ene_max_rated
        pro_forma.loc['CAPEX Year', 'Storage Capital Cost'] = -ccost_0

        fixed_om = tech.fixedOM * tech.dis_max_rated
        pro_forma.loc[self.start_year:, 'Storage Fixed O&M'] = np.repeat(fixed_om, len(pro_forma.index) - 1)

        for year in results.index.year.unique():
            mask = results.index.year == year  # select data for one year at a time
            tot_energy_delievered_es = results[mask]['dis'].values.sum() * len(results.index) * self.dt
            pro_forma.loc[pd.Period(year=year,
                                    freq='y'), 'Storage Variable O&M'] = tot_energy_delievered_es * tech.OMexpenses / 1000

        # calculate benefit for each service
        if "DCM" in services.keys():
            for year in self.adv_monthly_bill.index.year.unique():
                year_monthly = self.adv_monthly_bill[self.adv_monthly_bill.index.year == year]

                new_demand_charge = year_monthly['Demand Charge ($)'].values.sum()
                orig_demand_charge = year_monthly['Original Demand Charge ($)'].values.sum()
                pro_forma.loc[pd.Period(year=year, freq='y'), 'Avoided Demand Charge'] = orig_demand_charge - new_demand_charge
                services.pop('DCM')

        if "retailTimeShift" in services.keys():
            for year in results.index.year.unique():
                results_yearsub = results[results.index.year == year]
                p_energy = services['retailTimeShift'].p_energy
                # calculate energy cost every time step
                net_power = results_yearsub.loc[:, 'dis'] - results_yearsub.loc[:, 'ch'] - results_yearsub.loc[:,
                                                                                             'load'] + results_yearsub.loc[:,
                                                                                                       'generation']
                original_net_power = - results_yearsub.loc[:, 'load'] + results_yearsub.loc[:, 'generation']
                new_energy_charge = np.sum(-self.dt * np.multiply(net_power, p_energy))
                orig_energy_charge = np.sum(-self.dt * np.multiply(original_net_power,p_energy))
                pro_forma.loc[
                    pd.Period(year=year, freq='y'), 'Avoided Energy Charge'] = orig_energy_charge - new_energy_charge

            services.pop('retailTimeShift')
        # collect costs of all other services
        for service in services.keys():  # TODO: this needs to be built out
            pass

        # list of all the financials that are constant
        const_col = ['Storage Fixed O&M']
        # list of financials that are zero unless already specified
        zero_col = ['Storage Capital Cost']
        # the rest of columns should grow year over year
        growth_col = list(set(list(pro_forma)) - set(const_col) - set(zero_col))

        # set the 'CAPEX Year' row to all zeros
        pro_forma.loc['CAPEX Year', growth_col + const_col] = np.zeros(len(growth_col + const_col))
        # use linear interpolation for growth in between optimization years
        pro_forma[growth_col] = pro_forma[growth_col].apply(lambda x: x.interpolate(method='linear', limit_area='inside'), axis=0)

        if use_inflation:
            # forward fill growth columns with inflation
            last_sim = max(self.opt_years)
            for yr in pd.period_range(start=last_sim + 1, end=self.end_year, freq='y'):
                pro_forma.loc[yr, growth_col] = pro_forma.loc[yr - 1, growth_col] * (1 + self.inflation_rate / 100)
            # backfill growth columns (needed for year 0)
            pro_forma[growth_col] = pro_forma[growth_col].fillna(value=0)
            # fill in constant columns
            pro_forma[const_col] = pro_forma[const_col].fillna(method='ffill')
            # fill in zero columns
            pro_forma[zero_col] = pro_forma[zero_col].fillna(value=0)

        else:
            # extrapolate TODO Test this
            pro_forma[growth_col] = pro_forma[growth_col].apply(lambda x: x.interpolate(method='polynomial', order=3, limit_area='outside'),
                                                                axis=0)  # is this what we want???
            pro_forma = pro_forma.interpolate(method='linear', limit_area='inside')  # is this what we want???
            pro_forma = pro_forma.interpolate(method='polynomial', order=3, limit_area='outside')  # is this what we want???
            # fill in fixed O&M columns
            pro_forma[const_col] = pro_forma[const_col].fillna(method='ffill')
            # fill in zero columns
            pro_forma[zero_col] = pro_forma[zero_col].fillna(value=0)

        # prepare for cost benefit (we dont want to include net values, so we do this first)
        cost_df = pd.DataFrame(pro_forma.values.clip(max=0))
        cost_df.columns = pro_forma.columns
        benefit_df = pd.DataFrame(pro_forma.values.clip(min=0))
        benefit_df.columns = pro_forma.columns

        # calculate the net (sum of the row's columns)
        pro_forma['Yearly Net Value'] = pro_forma.sum(axis=1)
        self.pro_forma = pro_forma

        # CALCULATING NET PRESENT VALUES
        # use discount rate to calculate NPV for net
        discount_rate = self.npv_discount_rate / 100
        npv_dict = {}
        # NPV for growth_cols
        for col in pro_forma.columns:
            npv_dict.update({col: [np.npv(discount_rate / 100, pro_forma[col].values)]})
        self.npv = pd.DataFrame(npv_dict, index=pd.Index(['NPV']))

        # CALCULATING COST-BENEFIT TABLE
        cost_pv = 0  # cost present value (discounted cost)
        benefit_pv = 0  # benefit present value (discounted benefit)
        self.cost_benefit = pd.DataFrame({'Lifetime Present Value': [0, 0]}, index=pd.Index(['Cost ($)', 'Benefit ($)']))
        for col in cost_df.columns:
            present_cost = np.npv(discount_rate, cost_df[col].values)
            present_benefit = np.npv(discount_rate, benefit_df[col].values)

            self.cost_benefit[col] = [np.abs(present_cost), present_benefit]

            cost_pv += present_cost
            benefit_pv += present_benefit
        self.cost_benefit['Lifetime Present Value'] = [np.abs(cost_pv), benefit_pv]

    def add_price_growth(self, rate):
        """ Updates the growth_rates attribute with price related, which will be used within the add_growth function.

        Args:
            rate (Dict): key is the name of item of which the rate applies to its price (ie. DA or FR), and the value is the rate value

        """
        self.growth_rates.update(rate)

    def __eq__(self, other, compare_init=False):
        """ Determines whether Analysis object equals another Analysis object. Compare_init = True will do an initial
        comparison ignoring any attributes that are changed in the course of running a case.

        Args:
            other (Analysis): Analysis object to compare
            compare_init (bool): Flag to ignore attributes that change after initialization

        Returns:
            bool: True if objects are close to equal, False if not equal.
        """
        return sh.compare_class(self, other, compare_init)
