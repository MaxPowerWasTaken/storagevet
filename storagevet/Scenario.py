"""
Scenario.py

This Python class contains methods and attributes vital for completing the scenario analysis.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.DAEnergyTimeShift import DAEnergyTimeShift
from ValueStreams.FrequencyRegulation import FrequencyRegulation
from ValueStreams.NonspinningReserve import NonspinningReserve
from ValueStreams.DemandChargeReduction import DemandChargeReduction
from ValueStreams.EnergyTimeShift import EnergyTimeShift
from ValueStreams.SpinningReserve import SpinningReserve
from ValueStreams.Backup import Backup
from ValueStreams.Deferral import Deferral
from ValueStreams.DemandResponse import DemandResponse
from ValueStreams.ResourceAdequacy import ResourceAdequacy
from ValueStreams.UserConstraints import UserConstraints
from ValueStreams.VoltVar import VoltVar
from Technology.BatteryTech import BatteryTech
from Technology.CAESTech import CAESTech
from Technology.NoCurtailPV import NoCurtailPV
import numpy as np
import pandas as pd
import Finances as Fin
import cvxpy as cvx
import Library as Lib
from prettytable import PrettyTable
import time
import sys
import copy
import logging
from datetime import datetime

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class Scenario(object):
    """ A scenario is one simulation run in the model_parameters file.

    """

    def __init__(self, input_tree):
        """ Initialize a scenario.

        Args:
            input_tree (Dict): Dict of input attributes such as time_series, params, and monthly_data

        """

        self.verbose = input_tree.Scenario['verbose']
        dLogger.info("Creating Scenario...")

        self.active_objects = input_tree.active_components
        # keys are: 'pre-dispatch', 'service', 'load', 'generator', 'storage', 'technologies', 'finance', 'scenario'

        self.start_time = time.time()
        self.start_time_frmt = time.strftime('%Y%m%d%H%M%S')
        self.end_time = 0

        # add general case params (USER INPUTS)
        self.dt = input_tree.Scenario['dt']
        self.verbose_opt = input_tree.Scenario['verbose_opt']
        self.n = input_tree.Scenario['n']
        self.n_control = input_tree.Scenario['n_control']
        self.mpc = input_tree.Scenario['mpc']

        self.start_year = input_tree.Scenario['start_year']
        self.end_year = input_tree.Scenario['end_year']
        self.opt_years = input_tree.Scenario['opt_years']
        self.incl_site_load = input_tree.Scenario['incl_site_load']
        self.incl_aux_load = input_tree.Scenario['incl_aux_load']
        self.incl_binary = input_tree.Scenario['binary']
        self.incl_slack = input_tree.Scenario['slack']
        # self.growth_rates = input_tree.Scenario['power_growth_rates']
        self.growth_rates = {'default': input_tree.Scenario['def_growth']}  # TODO: eventually turn into attribute
        self.frequency = input_tree.Scenario['frequency']

        # save loaded data
        self.time_series = input_tree.Scenario['time_series']  # goal is not to save this as an attribute (data should be saved within objects)
        self.cycle_life = input_tree.Scenario['cycle_life']
        self.monthly_data = input_tree.Scenario['monthly_data']

        self.customer_sided = input_tree.Scenario['customer_sided']

        self.technology_inputs_map = {
            'CAES': input_tree.CAES,
            'Storage': input_tree.Storage,
            'Battery': input_tree.Battery,
            'PV': input_tree.PV,
        }

        self.predispatch_service_inputs_map = {
            'Deferral': input_tree.Deferral,
            'DR': input_tree.DR,
            'RA': input_tree.RA,
            'Backup': input_tree.Backup,
            'Volt': input_tree.Volt,
            'User': input_tree.User
        }
        self.service_input_map = {
            'DA': input_tree.DA,
            'FR': input_tree.FR,
            'SR': input_tree.SR,
            'NSR': input_tree.NSR,
            'DCM': input_tree.DCM,
            'retailTimeShift': input_tree.retailTimeShift
        }

        self.solvers = set()

        # internal attributes to Case
        self.services = {}
        self.predispatch_services = {}
        self.technologies = {}
        self.financials = None

        if "Results_Directory" in self.active_objects['command']:
            # self.results_path = input_tree.csv_path + '/Results/' + '_' + self.start_time_frmt
            self.results_path = input_tree.csv_path
        else:
            # self.results_path = './Results/' + '_' + self.start_time_frmt
            self.results_path = './Results/' + '_' + datetime.now().strftime('%H_%M_%S_%m_%d_%Y')

        # setup outputs
        self.results = pd.DataFrame()

        self.power_kw = self.prep_opt_results(input_tree.Scenario['power_timeseries'])
        self.calculate_gen_load_data()
        self.init_financials(input_tree.Finance)

        dLogger.info("Scenario Created Successfully...")

    def prep_opt_results(self, time_series):
        """
        Create standard dataframe of power data from user time_series inputs

         Args:
            time_series (DataFrame): user time series data read from CSV
        Return:
            inputs_df (DataFrame): the required timeseries columns from the given time_series dataframe

        TODO: move this into Input
        """

        inputs_df = time_series

        # calculate data for simulation of future years using growth rate
        inputs_df = self.add_growth_data(inputs_df, self.opt_years, self.verbose)
        # for service in self.services

        # create opt_agg (has to happen after adding future years)
        if self.mpc:
            inputs_df = Lib.create_opt_agg(inputs_df, 'mpc', self.dt)
        else:
            inputs_df = Lib.create_opt_agg(inputs_df, self.n, self.dt)

        dLogger.info("Finished preparing optimization results.")
        return inputs_df

    def calculate_gen_load_data(self):
        """Add individual generation data to general generation columns, calculated from the inputted data.

        """
        self.power_kw['generation'] = 0
        if 'PV' in self.technologies.keys():
            self.power_kw['generation'] += self.technologies['PV'].generation

        # logic to exclude site or aux load from load used
        self.power_kw['load'] = 0
        if self.incl_site_load:
            self.power_kw['load'] += self.power_kw['Site_Load (kW)']
        if self.incl_aux_load:
            self.power_kw['load'] += self.power_kw['Aux_Load (kW)']

        self.power_kw = self.power_kw.sort_index()

    def add_growth_data(self, df, opt_years, verbose=False):
        """ Helper function: Adds rows to df where missing opt_years

        Args:
            df (DataFrame): given data
            opt_years (List): List of Period years where we need data for
            verbose (bool):

        Returns:
            df (DataFrame):

        TODO:
            might be a good idea to move back to Library
            change this to work with OOP framework
        """

        data_year = df.index.year.unique()  # which years was data given for
        # data_year = [item.year for item in data_year]
        no_data_year = {pd.Period(year) for year in opt_years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        # if there is a year we dont have data for
        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))  # which year to to apply growth rate to (is this the logic we want??)

                # create new dataframe for missing year
                new_index = pd.date_range(start='01/01/' + str(yr), end='01/01/' + str(yr + 1), freq=self.frequency, closed='left')
                new_data = pd.DataFrame(index=new_index)

                source_data = df[df.index.year == source_year.year]  # use source year data

                def_rate = self.growth_rates['default']
                growth_cols = set(list(df))

                # for each column in growth column
                for col in growth_cols:
                    # look for specific growth rate in params, else use default growth rate
                    name = col.split(sep='_')[0].lower()
                    col_type = col.split(sep='_')[1].lower()
                    if col_type == 'load' or col_type == 'gen':
                        if name in self.growth_rates.keys():
                            rate = self.growth_rates[name]
                        else:
                            dLogger.info(str(name) + ' growth not in params. Using default growth rate:' + str(def_rate))
                            rate = def_rate
                    elif col_type == 'price':
                        if name in self.financials.growth_rates.keys():
                            rate = self.financials.growth_rates[name]
                        else:
                            dLogger.info((name, ' growth not in params. Using default growth rate:', def_rate))
                            rate = def_rate
                    else:
                        rate = 0
                    new_data[col] = Lib.apply_growth(source_data[col], rate, source_year, yr, self.frequency)  # apply growth rate to column

                # add new year to original data frame
                df = pd.concat([df, new_data], sort=True)

        return df

    def init_financials(self, finance_inputs):
        """ Initializes the financial class with a copy of all the price data from timeseries, the tariff data, and any
         system variables required for post optimization analysis.

         Args:
             finance_inputs (Dict): Financial inputs

        """

        self.financials = Fin.Financial(finance_inputs)
        dLogger.info("Finished adding Financials...")

    def add_technology(self):
        """ Reads params and adds technology. Each technology gets initialized and their physical constraints are found.

        """
        ess_action_map = {
            'Battery': BatteryTech,
            'CAES': CAESTech
        }

        active_storage = self.active_objects['storage']
        storage_inputs = self.technology_inputs_map['Storage']
        for storage in active_storage:
            inputs = self.technology_inputs_map[storage]
            tech_func = ess_action_map[storage]
            self.technologies['Storage'] = tech_func('Storage', self.power_kw['opt_agg'], inputs, storage_inputs, self.cycle_life)
            dLogger.info("Finished adding storage...")

        generator_action_map = {
            'PV': NoCurtailPV
        }

        active_gen = self.active_objects['generator']
        for gen in active_gen:
            inputs = self.technology_inputs_map[gen]
            tech_func = generator_action_map[gen]
            self.technologies[gen] = tech_func(gen, inputs)
            dLogger.info("Finished adding generators...")

        self.power_kw['generation'] = 0
        if 'PV' in self.active_objects['generator']:
            self.power_kw['generation'] += self.technologies['PV'].generation

        dLogger.info("Finished adding active Technologies...")

    def add_services(self):
        """ Reads through params to determine which services are turned on or off. Then creates the corresponding
        service object and adds it to the list of services. Also generates a list of growth functions that apply to each
        service's timeseries data (to be used when adding growth data).

        Notes:
            This method needs to be applied after the technology has been initialized.
            ALL SERVICES ARE CONNECTED TO THE TECH

        TODO [multi-tech] need dynamic mapping of services to tech in RIVET
        """
        storage_inputs = self.technologies['Storage']

        predispatch_service_action_map = {
            'Deferral': Deferral,
            'DR': DemandResponse,
            'RA': ResourceAdequacy,
            'Backup': Backup,
            'Volt': VoltVar,
            'User': UserConstraints
        }
        for service in self.active_objects['pre-dispatch']:
            dLogger.info("Using: " + str(service))
            inputs = self.predispatch_service_inputs_map[service]
            service_func = predispatch_service_action_map[service]
            new_service = service_func(inputs, storage_inputs, self.power_kw, self.dt)
            new_service.estimate_year_data(self.opt_years, self.frequency)
            self.predispatch_services[service] = new_service

        dLogger.info("Finished adding Predispatch Services for Value Stream")

        service_action_map = {
            'DA': DAEnergyTimeShift,
            'FR': FrequencyRegulation,
            'SR': SpinningReserve,
            'NSR': NonspinningReserve,
            'DCM': DemandChargeReduction,
            'retailTimeShift': EnergyTimeShift
        }

        for service in self.active_objects['service']:
            dLogger.info("Using: " + str(service))
            inputs = self.service_input_map[service]
            service_func = service_action_map[service]
            new_service = service_func(inputs, storage_inputs, self.dt)
            new_service.estimate_year_data(self.opt_years, self.frequency)
            self.services[service] = new_service

        dLogger.info("Finished adding Services for Value Stream")

        if 'Deferral' in self.active_objects['pre-dispatch']:
            self.check_for_deferral_failure()

    def check_for_deferral_failure(self):
        """ This functions checks the constraints of the storage system against any predispatch or user inputted constraints
        for any infeasible constraints on the system.

        Only runs if Deferral is active.

        """
        dLogger.info('Finding first year of deferral failure...')
        deferral = self.predispatch_services['Deferral']
        tech = self.technologies['Storage']
        rte = tech.rte
        max_ch = tech.ch_max_rated
        max_dis = tech.dis_max_rated
        max_ene = tech.ene_max_rated * tech.ulsoc
        current_year = self.power_kw.index.year[-1]
        failed_year = 0
        last_deferred_yr = 0
        while failed_year == 0:
            generation = self.power_kw['generation']
            load = self.power_kw['load']
            min_power, min_energy = deferral.precheck_failure(self.dt, rte, load, generation)
            print('min power : ' + str(min_power))
            print('min energy: ' + str(min_energy))
            if min_power > max_ch or min_power > max_dis or min_energy > max_ene:
                failed_year = current_year
                last_deferred_yr = current_year - 1
                deferral.set_last_deferral_year(last_deferred_yr)
            elif current_year == self.end_year.year:  # if end of analysis year is reached before deferral failure is found
                uLogger.warning('Deferral does not fail within the last year of simulation.\n')
                break
            else:
                uLogger.info('Deferral is not predicted to fail in ' + str(current_year) + ' at first look.')
                uLogger.info('Adding another year of data...' + str(current_year))
                current_year += 1

                # remove columns that were not included in original timeseries (only pv and loads left)
                original_timeseries_headers = set(self.power_kw) - {'opt_agg', 'generation', 'load'}
                power_kw_temp = self.power_kw.loc[:, original_timeseries_headers]

                new_opt_years = self.opt_years + [current_year]
                self.power_kw = self.add_growth_data(power_kw_temp, new_opt_years, False)

                # add year of data to each value stream
                for service in self.services.values():
                    service.estimate_year_data(new_opt_years, self.frequency)

                for service in self.predispatch_services.values():
                    service.estimate_year_data(new_opt_years, self.frequency)

                self.calculate_gen_load_data()

        if not failed_year:
            dLogger.info('Check for Infeasibility: No new years are added to simulation.')
        else:
            if last_deferred_yr > min(self.opt_years) and last_deferred_yr not in self.opt_years:
                self.opt_years += [last_deferred_yr]
                if failed_year not in self.opt_years:
                    self.opt_years += [failed_year]
            if failed_year > min(self.opt_years) and failed_year not in self.opt_years:
                self.opt_years += [failed_year]

        # remove any years to not be used
        for service in self.services.values():
            service.estimate_year_data(self.opt_years, self.frequency)

        for service in self.predispatch_services.values():
            service.estimate_year_data(self.opt_years, self.frequency)

        # print years that optimization will run for
        opt_years_str = '[  '
        for year in self.opt_years:
            opt_years_str += str(year) + '  '
        opt_years_str += ']'
        dLogger.info('Running analysis on years: ' + opt_years_str)

        # keep only the data for the years that the optimization will run on
        temp_df = pd.DataFrame()
        for year in self.opt_years:
            temp_df = pd.concat([temp_df, self.power_kw[self.power_kw.index.year == year]])
        self.power_kw = temp_df

        # create opt_agg (has to happen after adding future years)
        if self.mpc:
            self.power_kw = Lib.create_opt_agg(self.power_kw, 'mpc', self.dt)
        else:
            self.power_kw = Lib.create_opt_agg(self.power_kw, self.n, self.dt)

        # add growth data for prices
        # self.financials.update_price_data(self.opt_years)

        # for name in self.active_objects['technology']:
        # self.technologies['Storage'].update_data(self.financials.fin_inputs)

    def add_control_constraints(self, deferral_check=False):
        """ Creates time series control constraints for each technology based on predispatch services.
        Must be run after predispatch services are attached to case.

        Args:
            deferral_check (bool): flag to return non feasible timestamps if running deferral feasbility analysis

        """
        tech = self.technologies['Storage']
        for service in self.predispatch_services.values():
            tech.add_value_streams(service, predispatch=True)
        feasible_check = tech.calculate_control_constraints(self.power_kw.index)  # should pass any user inputted constraints here

        if (feasible_check is not None) & (not deferral_check):
            # if not running deferral failure analysis and infeasible scenario then stop and tell user
            dLogger.error('Predispatch and Storage inputs results in infeasible scenario')
            quit()
        elif deferral_check:
            # return failure dttm to deferral failure analysis
            dLogger.info('Returned feasible_check to deferral failure analysis.')
            return feasible_check
        else:
            dLogger.info("Control Constraints Successfully Created...")

    def optimize_problem_loop(self):
        """ This function selects on opt_agg of data in self.time_series and calls optimization_problem on it.

        """

        dLogger.info("Preparing Optimization Problem...")
        uLogger.info("Preparing Optimization Problem...")

        # list of all optimization windows
        periods = pd.Series(copy.deepcopy(self.power_kw.opt_agg.unique()))
        periods.sort_values()

        # for mpc
        window_shift = 0

        for ind, opt_period in enumerate(periods):
            # ind = 0
            # opt_period = self.power_kw.opt_agg[ind]

            # used to select rows from time_series relevant to this optimization window
            if not self.mpc:
                mask = self.power_kw.loc[:, 'opt_agg'] == opt_period
                # mask_index =
            else:
                mask = self.power_kw['opt_agg'].between(1 + int(self.n_control) * window_shift, int(self.n) + int(self.n_control) * window_shift)
                window_shift += 1

            # apply past degradation
            storage = self.technologies['Storage']
            storage.apply_past_degredation(ind, self.power_kw, mask, opt_period, self.n)

            print(time.strftime('%H:%M:%S') + ": Running Optimization Problem for " + str(self.power_kw.loc[mask].index[0]) + "...")

            # run optimization and return optimal variable and objective costs
            results, obj_exp = self.optimization_problem(mask)

            # Add past degrade rate with degrade from calculated period
            storage = self.technologies['Storage']
            storage.calc_degradation(opt_period, results.index[0], results.index[-1], results['ene'])

            # add optimization variable results to power_kw
            if not results.empty and not self.mpc:
                self.power_kw = Lib.update_df(self.power_kw, results)
            elif not results.empty and self.mpc:
                results = results[:int(self.n_control)]
                self.power_kw = Lib.update_df(self.power_kw, results)

            # # add objective costs to financial obj_val
            # if not obj_exp.empty:
            #     obj_exp.index = [opt_period]
            #     self.financials.obj_val = Lib.update_df(self.financials.obj_val, obj_exp)

            if self.mpc:
                if not self.power_kw['ene'].isnull().values.any():
                    self.financials.obj_val = self.financials.obj_val.iloc[:window_shift]
                    break

    def optimization_problem(self, mask):
        """ Sets up and runs optimization on a subset of data.

        Args:
            mask (DataFrame): DataFrame of booleans used, the same length as self.time_series. The value is true if the
                        corresponding column in self.time_series is included in the data to be optimized.

        Returns:
            variable_values (DataFrame): Optimal dispatch variables for each timestep in optimization period.
            obj_values (Data Frame): Objective costs representing the financials of each service for the optimization period.

        """

        opt_var_size = int(np.sum(mask))

        # subset of input data relevant to this optimization period
        subs = self.power_kw.loc[mask, :]
        uLogger.info(subs.index.year[0])

        obj_const = []  # list of constraint costs (make this a dict for continuity??)
        variable_dic = {}  # Dict of optimization variables
        obj_expression = {}  # Dict of objective costs

        # add optimization variables for each technology
        # TODO [multi-tech] need to handle naming multiple optimization variables (i.e ch_1)
        for tech in self.technologies.values():
            variable_dic.update(tech.add_vars(opt_var_size))

        # default power and energy reservations (these could be filled in with optimization variables or costs below)
        power_reservations = np.array([0, 0, 0, 0])  # [c_max, c_min, d_max, d_min]
        energy_reservations = [cvx.Parameter(shape=opt_var_size, value=np.zeros(opt_var_size), name='zero'),
                               cvx.Parameter(shape=opt_var_size, value=np.zeros(opt_var_size), name='zero'),
                               cvx.Parameter(shape=opt_var_size, value=np.zeros(opt_var_size), name='zero')]  # [e_upper, e, e_lower]

        # add any constraints added by pre-dispatch services (currently only if DEFERRAL)

        for predispatch_serv in self.predispatch_services.values():
            obj_const += predispatch_serv.build_constraints(variable_dic, subs)

        for service in self.services.values():
            variable_dic.update(service.add_vars(opt_var_size))  # add optimization variables associated with each service
            obj_expression.update(service.objective_function(variable_dic, subs))  # add objective expression associated with each service
            obj_const += service.build_constraints(variable_dic, subs)   # add constraints associated with each service
            temp_power, temp_energy = service.power_ene_reservations(variable_dic)
            power_reservations = power_reservations + np.array(temp_power)   # add power reservations associated with each service
            energy_reservations = energy_reservations + np.array(temp_energy)   # add energy reservations associated with each service

        reservations = {'C_max': power_reservations[0],
                        'C_min': power_reservations[1],
                        'D_max': power_reservations[2],
                        'D_min': power_reservations[3],
                        'E': energy_reservations[1],
                        'E_upper': energy_reservations[0],
                        'E_lower': energy_reservations[2]}

        # add any objective costs from tech and the main physical constraints
        for tech in self.technologies.values():
            obj_expression.update(tech.objective_function(variable_dic, mask))

            if self.mpc:
                try:
                    ene = self.power_kw['ene'].dropna().iloc[-1]
                except KeyError:
                    ene = None

                obj_const += tech.build_master_constraints(variable_dic, mask, reservations, ene)
            else:
                obj_const += tech.build_master_constraints(variable_dic, mask, reservations)
        obj = cvx.Minimize(sum(obj_expression.values()))
        prob = cvx.Problem(obj, obj_const)
        dLogger.info("Finished setting up the problem. Solving now.")

        try:
            if prob.is_mixed_integer():
                # MBL: GLPK will solver to a default tolerance but can take a long time. Can use ECOS_BB which uses a branch and bound method
                # and input a tolerance but I have found that this is a more sub-optimal solution. Would like to test with Gurobi
                # information on solvers and parameters here: https://www.cvxpy.org/tgitstatutorial/advanced/index.html

                # prob.solve(verbose=self.verbose_opt, solver=cvx.ECOS_BB, mi_abs_eps=1, mi_rel_eps=1e-2, mi_max_iters=1000)
                start = time.time()
                prob.solve(verbose=self.verbose_opt, solver=cvx.GLPK_MI)
                end = time.time()
                dLogger.info("Time it takes for solver to finish: " + str(end - start))
            else:
                start = time.time()
                # ECOS is default sovler and seems to work fine here
                prob.solve(verbose=self.verbose_opt)
                end = time.time()
                dLogger.info("ecos solver")
                dLogger.info("Time it takes for solver to finish: " + str(end - start))
        except cvx.error.SolverError as err:  # does this actually catch anything?
            uLogger.error("Solver Error...")
            dLogger.error("Solver Error...")
            sys.exit(err)

        uLogger.info(prob.status)

        # save solver used
        self.solvers = self.solvers.union(prob.solver_stats.solver_name)

        cvx_types = (cvx.expressions.cvxtypes.expression(), cvx.expressions.cvxtypes.constant())
        # evaluate optimal objective expression
        obj_values = pd.DataFrame({name: [obj_expression[name].value if isinstance(obj_expression[name], cvx_types) else obj_expression[name]] for name in list(obj_expression)})
        # collect optimal dispatch variables
        variable_values = pd.DataFrame({name: variable_dic[name].value for name in list(variable_dic)}, index=subs.index)

        # check for non zero slack
        if np.any(abs(obj_values.filter(regex="_*slack$")) >= 1):
            uLogger.info('WARNING! non-zero slack variables found in optimization solution')

        # check for charging and discharging in same time step
        eps = 1e-4
        if any(((abs(variable_values['ch']) >= eps) & (abs(variable_values['dis']) >= eps)) & ('CAES' not in self.active_objects['storage'])):
            uLogger.info('WARNING! non-zero charge and discharge powers found in optimization solution. Try binary formulation')

        # collect actual energy contributions from services
        for serv in self.services.values():
            if self.customer_sided:
                temp_ene_df = pd.DataFrame({'ene': np.zeros(len(subs.index))}, index=subs.index)
            else:
                sub_list = serv.e[-1].value.flatten('F')
                temp_ene_df = pd.DataFrame({'ene': sub_list}, index=subs.index)
            serv.ene_results = pd.concat([serv.ene_results, temp_ene_df], sort=True)
        return variable_values, obj_values

    def __eq__(self, other, compare_init=False):
        """ Determines whether case object equals another case object. Compare_init = True will do an initial comparison
        ignoring any attributes that are changed in the course of running a case.

        Args:
            other (Case): Case object to compare
            compare_init (bool): Flag to ignore attributes that change after initialization

        Returns:
            bool: True if objects are close to equal, False if not equal.
        """
        return Lib.compare_class(self, other, compare_init)

    @staticmethod
    def search_schema_type(root, attribute_name):
        for child in root:
            attributes = child.attrib
            if attributes.get('name') == attribute_name:
                if attributes.get('type') == None:
                    return "other"
                else:
                    return attributes.get('type')

    # TODO: this summary is for a specific scenario - TN
    def instance_summary(self, input_tree):
        tree = input_tree.xmlTree
        treeRoot = tree.getroot()
        schema = input_tree.schema_tree

        dLogger.info("Printing summary table for each scenario...")
        table = PrettyTable()
        table.field_names = ["Category", "Element", "Active?", "Property", "Analysis?",
                             "Value", "Value Type", "Sensitivity"]
        for element in treeRoot:
            schemaType = self.search_schema_type(schema.getroot(), element.tag)
            activeness = element.attrib.get('active')
            for property in element:
                table.add_row([schemaType, element.tag, activeness, property.tag, property.attrib.get('analysis'),
                        property.find('Value').text, property.find('Type').text, property.find('Sensitivity').text])

        uLogger.info("\n" + str(table))
        dLogger.info("Successfully printed summary table for class Scenario in log file")

        return 0
