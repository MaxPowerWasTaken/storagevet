"""
Load.py

"""

__author__ = 'Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani',
               'Micah Botkin-Levy', "Thien Nguyen", 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']


import xml.etree.ElementTree as et
import pandas as pd
import sys
import ast
import itertools
import logging
import copy
import csv
import Library as sh
import numpy as np
from prettytable import PrettyTable
from datetime import datetime
import collections

import matplotlib.pyplot as plt
from pandas import read_csv


class Load:
    """
        This class specifies the components of a billing period that describes energy rates as defined in the
        OpenEI US Utility Rate Database (USURDB). More information and rates: https://openei.org/apps/USURDB/?
    """

    def __init__(self):
        self.timeseries = None
        self.load_factor = None
        self.average = None
        self.maximum = None
        self.minimum = None
        self.start = None
        self.end = None
        self.dt = None

    def set_profile(self, series): # this is ideally a pandas Series from a dataframe or standalone
        self.timeseries = series
        return # need to setup all the attributes here ... init will eventually call this

    def get_profile(self):
        return self.timeseries if self.timeseries is not None else None
