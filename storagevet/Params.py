"""
Params.py

"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani',
               'Micah Botkin-Levy', "Thien Nguyen", 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']


import xml.etree.ElementTree as et
from pathlib import Path
import pandas as pd
import sys
import ast
import itertools
import logging
import copy
import Library as sh
import numpy as np
from prettytable import PrettyTable
from datetime import datetime
import os

import matplotlib.pyplot as plt
from pandas import read_csv

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class Params:
    """
        Class attributes are made up of services, technology, and any other needed inputs. The attributes are filled
        by converting the xml file in a python object.

        Notes:
             Need to change the summary functions for pre-visualization every time the Params class is changed - TN
    """

    @classmethod
    def initialize(cls, filename, schema):
        """
            Initialize the class variable of the Params class that will be used to create Params objects for the
            sensitivity analyses. Specifically, it will preload the needed CSV and/or time series data, identify
            sensitivity variables, and prepare so-called default Params values as a template for creating objects.

            Args:
                filename (string): filename of XML file to load for StorageVET sensitivity analysis
                schema (string): schema file name
        """
        dLogger.info('Initializing the XML tree with the provided file...')
        cls.xmlTree = et.parse(filename)
        cls.schema_tree = et.parse(schema)
        
        cls.active_components = {"pre-dispatch": list(), "service": list(), "generator": list(), "load": list(),
                                 "storage": list(), "der": list(), "scenario": list(),
                                 "finance": list(), "command": list()}

        cls.sensitivity = {"attributes": dict(), "coupled": list()}  # contains all sensitivity variables as keys and their values as lists
        cls.df_analysis = pd.DataFrame()  # Each row specifies the value of each attribute
        cls.instances = dict()
        cls.datasets = {"time_series": dict(),
                        "monthly_data": dict(),
                        "customer_tariff": dict(),
                        "cycle_life": dict()}                               # for a scenario in the sensitivity analysis
        # holds the data of all the time series usd in sensitivity analysis

        dLogger.info('Updating data for Sensitivity Analysis...')
        cls.fetch_sense()
        if cls.sensitivity["attributes"]:
            # add a function to determine if there is coupled among the sensitivity attributes
            # possible to add an argument for build_dataframe function to check if there is coupling?
            cls.build_dataframe()

        cls.data_prep()
        cls.template = cls()
        try:
            cls.template.load_data_sets()
        except:
            dLogger.error("data cannot be loaded")
            sys.exit()

        if cls.sensitivity["attributes"]:
            cls.input_object_builder()
        else:
            cls.instances[0] = cls.template

    def __init__(self):
        """ Initialize these following attributes of the empty Params class object.
        """
        # attributes shared with DERVET
        self.DCM = self.read_from_xml_object('DCM')
        self.retailTimeShift = self.read_from_xml_object('retailTimeShift')

        self.PV = self.read_from_xml_object('PV')
        self.Battery = self.read_from_xml_object('Battery')  # this is an empty dictionary
        self.Storage = self.read_from_xml_object('Storage')

        self.Scenario = self.read_from_xml_object('Scenario')
        self.Finance = self.read_from_xml_object('Finance')
        self.Pre = self.read_from_xml_object('Previsualization')  # this is an empty dictionary
        self.Sim = self.read_from_xml_object('Simulation')  # this is an empty dictionary
        self.Val = self.read_from_xml_object('Validation')  # this is an empty dictionary
        self.Path = self.read_from_xml_object('Results_Directory')
        self.Dispatch = {}  # this will be over written by DERVET (None if Sizing)
        if self.Dispatch is not None:
            # value-streams
            self.Backup = self.read_from_xml_object('Backup')  # this is an empty dictionary
            self.DA = self.read_from_xml_object('DA')
            self.Deferral = self.read_from_xml_object('Deferral')
            self.DR = self.read_from_xml_object('DR')
            self.Flexr = self.read_from_xml_object('Flexr')
            self.FR = self.read_from_xml_object('FR')
            self.NSR = self.read_from_xml_object('NSR')
            self.RA = self.read_from_xml_object('RA')
            self.RT = self.read_from_xml_object('RT')
            self.SR = self.read_from_xml_object('SR')
            self.User = self.read_from_xml_object('User')
            self.Volt = self.read_from_xml_object('Volt')

            self.CAES = self.read_from_xml_object('CAES')

    @classmethod
    def read_from_xml_object(cls, name, flag=False):
        """ Read data from xml file.

            Args:
                name (str): name of the root element in the xml file
                flag (bool):

            Returns:
                A dictionary filled with needed given values of the properties of each service/technology
                if the service or technology is not implemented in this case, then None is returned.
        """

        component = cls.xmlTree.find(name)

        # this catches a misspelling in the Params 'name' compared to the xml trees spelling of 'name'
        if component is None:
            return None

        # This statement checks if the first character is 'y' or '1', if true it creates a dictionary.
        if component.get('active')[0].lower() == "y" or component.get('active')[0] == "1":
            dictionary = {}
            cls.fill_active(component.tag)
            for properties in component:
                # fills dictionary with the values from the xml file
                dictionary[properties.tag] = Params.convert_data_type(properties.find('Value').text,
                                                                     properties.find('Type').text)
                if flag and properties.get('analysis') is not None and properties.get('analysis')[0].lower() == "y":
                    temp = dictionary[properties.tag]
                    dictionary[properties.tag] = set(ast.literal_eval(properties.find('Sensitivity_Parameters').text))
                    dictionary[properties.tag].add(temp)
                if component.tag == 'Results_Directory':
                    cls.csv_path = properties.find('Value').text + '/Results/' + 'result_' + datetime.now().strftime('%H_%M_%S_%m_%d_%Y')
        else:
            return None

        return dictionary

    @classmethod
    def fill_active(cls, name):
        """ if given element is active it adds it to the class variable 'active_components'.
            checks to find the elements category (i.e. service and pre-dispatch) through the schema.

            Args:
                name (str): name of the root element in the Params xml file

            Returns:

        """
        root = cls.schema_tree.getroot().findall('*')

        for component in root:
            if component.get('name') is not None and component.get('name') == name:
                component_type = component.get('type')
                break

        if component_type == 'pre-dispatch':
            cls.active_components['pre-dispatch'].append(name)
        elif component_type == 'service':
            cls.active_components["service"].append(name)
        elif component_type == 'generator':
            cls.active_components["generator"].append(name)
        elif component_type == 'load':
            cls.active_components["load"].append(name)
        elif component_type == 'storage':
            cls.active_components["storage"].append(name)
        elif component_type == 'der':
            cls.active_components["der"].append(name)
        elif component_type == 'scenario':
            cls.active_components["scenario"].append(name)
        elif component_type == 'finance':
            cls.active_components["finance"].append(name)
        elif component_type == 'command':
            cls.active_components["command"].append(name)

    @classmethod
    def fetch_sense(cls):
        """
            Function to provide initialize the sensitivity variable of the Params class (if there is any)

            Args: none

            Returns: True

        """
        root = cls.xmlTree.getroot()

        for element in list(root):
            if element.get('active')[0].lower() != "y" and element.get('active')[0] != "1":
                continue

            for properties in list(element):
                if properties.get('analysis') is None or \
                        properties.get('analysis')[0].lower() != "y" and properties.get('analysis')[0] != "1":
                    continue

                cls.sensitivity["attributes"][(element.tag, properties.tag)] = \
                    cls.determine_sensitivity_list(element.tag, properties.tag)

        return True

    @classmethod
    def determine_sensitivity_list(cls, element, property):
        """
            Function to determine the list of values for the given element/component.
            They can have more than 1 values (if sensitivity is yes)

            Args:
                element (string): element is the same as the component
                property (string): attribute of the element

            Returns:

        """
        component = cls.xmlTree.find(element)
        attribute = component.find(property)
        if attribute.find('Sensitivity_Parameters').text is None or attribute.get('analysis')[0].lower() != "y" and attribute.get('analysis')[0] != "1":
            slist = cls.extract_data(attribute.find('Value').text, attribute.find('Type').text)
        else:
            slist = cls.extract_data(attribute.find('Sensitivity_Parameters').text, attribute.find('Type').text)
        return slist

    @classmethod
    def extract_data(cls, expression, dataType):
        """
            Function to extract out all the sensitivity or non-sensitvity values for each given expression,
            due to their string format. And then to convert these data values to the proper data types
            in prior to the return.

            Args:
                expression (string): the string of expression to be extracted
                dataType (string): the string of dataType

            Returns:

        """
        result = []
        expression = expression.strip()
        if expression.startswith("[") and expression.endswith("]"):
            expression = expression[1:-1]
        sset = expression.split(',')
        for s in sset:
            data = cls.convert_data_type(s.strip(), dataType)
            result.append(data)
        return result

    @classmethod
    def build_dataframe(cls):
        """
            Function to create a dataframe that includes all the possible combinations of sensitivity values

        """
        cls.df_analysis = pd.DataFrame(cls.sensitivity['attributes'])

        sense = cls.sensitivity["attributes"]
        keys, values = zip(*sense.items())
        experiments = [dict(zip(keys,v)) for v in itertools.product(*values)]

        cls.df_analysis = pd.DataFrame(experiments)

    @classmethod
    def input_object_builder(cls):
        """
            Function to create all the instances based on the possible combinations of sensitivity values provided
            To update the variable instances of this Params class object

            Args:

            Returns:

        """

        dictionary = {}
        for index, row in cls.df_analysis.iterrows():
            inputobject = copy.deepcopy(cls.template)
            for col in row.index:
                # TODO funny issue with pandas dataframe with all float gives numpy.float64
                # but float is wanted fixed with .astype(object) but might want to investigate -YY
                inputobject.modify_attribute(tupel=col, value=row[col])
            inputobject.load_data_sets()
            dictionary.update({index: inputobject})
        cls.instances = dictionary

    def modify_attribute(self, tupel, value):
        """
            Function to modify the value of the sensitivity value based on the specific instance/scenario

            Args:
                tupel (tuple): the group of attributes
                value (int/float): the new value to replace the former value

            Returns:
        """

        attribute = getattr(self, tupel[0])
        attribute[tupel[1]] = value

    @classmethod
    def data_prep(cls):
        """ error.

         Returns: True after completing

        """
        ts_files = set(cls.determine_sensitivity_list('Scenario', 'time_series_filename'))
        md_files = set(cls.determine_sensitivity_list('Scenario', 'monthly_data_filename'))
        ct_files = set(cls.determine_sensitivity_list('Scenario', 'customer_tariff_filename'))
        cl_files = set(cls.determine_sensitivity_list('Scenario', 'cycle_life_filename'))

        for ts_file in ts_files:
            cls.datasets['time_series'][ts_file] = cls.read_from_file('time_series', ts_file, 'Datetime', True)
        for md_file in md_files:
            cls.datasets['monthly_data'][md_file] = cls.preprocess_monthly(cls.read_from_file('monthly_data', md_file, ['Year', 'Month'], True))
        for ct_file in ct_files:
            cls.datasets['customer_tariff'][ct_file] = cls.read_from_file('customer_tariff', ct_file,  'Billing Period', verbose=True)
        for cl_file in cl_files:
            cls.datasets['cycle_life'][cl_file] = cls.read_from_file('cycle_life', cl_file, None, True)

        return True

    @staticmethod
    def read_from_file(name, filename, ind_col=None, verbose=False):
        """ Read data from csv or excel file.

        Args:
            name (str): name of data to read
            filename (str): filename of file to read
            ind_col (str or list): column(s) to use as dataframe index
            verbose (bool): flag to display statements

         Returns:
                A pandas dataframe of file at FILENAME location
        TODO convert string to float where possible

        """
        filename = Path('.').joinpath('Data', filename)

        raw = pd.DataFrame()

        if (filename is not None) and (not pd.isnull(filename)):

            # logic for time_series data
            parse_dates = name == 'time_series'
            infer_dttm = name == 'time_series'

            # select read function based on file type
            func = pd.read_csv if ".csv" in filename.name else pd.read_excel

            try:
                raw = func(filename, parse_dates=parse_dates, index_col=ind_col, infer_datetime_format=infer_dttm)
            except UnicodeDecodeError:
                try:
                    raw = func(filename, parse_dates=parse_dates, index_col=ind_col, infer_datetime_format=infer_dttm,
                               encoding="ISO-8859-1")
                except (ValueError, IOError):
                    dLogger.error("Could not open: ", filename)
                    quit()
                else:
                    dLogger.info("Successfully read in", filename)
            except (ValueError, IOError):
                # TODO improve error handling (i.e. if file is not there)
                dLogger.error("Could not open or could not find: ", filename)
                quit()
            else:
                dLogger.info("Successfully read in: " + filename.name)

        return raw

    @staticmethod
    def preprocess_monthly(monthly_data):
        """ processing monthly data.
        Creates monthly Period index from year and month

        Args:
            monthly_data (DataFrame): Raw df

        Returns:
            monthly_data (DataFrame): edited df

        """
        if not monthly_data.empty:
            monthly_data.index = pd.PeriodIndex(year=monthly_data.index.get_level_values(0).values,
                                                month=monthly_data.index.get_level_values(1).values, freq='M')
            monthly_data.index.name = 'yr_mo'
        return monthly_data

    @staticmethod
    def csv_to_xml(csv_filename):
        """ converts csv to xml

        Args:
            filename (string): name of csv file

        Returns:
            xmlFile (string): name of xml file

        """
        # find .csv in the filename and replace with .xml
        xml_filename = csv_filename[:csv_filename.rfind('.')] + ".xml"

        # open csv to read into dataframe and blank xml file to write to
        csvData = pd.read_csv(csv_filename)
        xmlData = open(xml_filename, 'w')

        # write the header of the xml file and specify columns to place in xml model parameters template
        xmlData.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
        xmlData.write('\n<input>\n')
        xmlColumns = ['Value', 'Units', 'Type', 'Allowed Values', 'Sensitivity Parameters', 'Coupled', 'Description']

        # outer loop for each tag/object and active status, i.e. Scenario, Battery, DA, etc.
        for obj in csvData.Tag.unique():
            mask = csvData.Tag == obj
            xmlData.write('\n    <' + obj + ' active="' + csvData[mask].Active.iloc[0] + '">\n')
            # middle loop for each object's elements and is sensitivity is needed: max_ch_rated, ene_rated, price, etc.
            for ind, row in csvData[mask].iterrows():
                if row['Key'] is pd.np.nan: continue
                xmlData.write('        <' + str(row['Key']) + ' analysis="' + str(row['Sensitivity Analysis']) + '">\n')
                # inner loop for specifying elements Value, Type, Units, etc.
                for tag in xmlColumns:
                    xmlData.write('            <' + str(tag).replace(' ','_') + '>' + str(row[tag]) + '</' + str(tag).replace(' ','_') + '>\n')
                xmlData.write('        </' + str(row['Key']) + '>\n')
            xmlData.write('    </' + obj + '>\n')
        xmlData.write('\n</input>')
        xmlData.close()

        return xml_filename

    @staticmethod
    def convert_data_type(value, desired_type):
        """ coverts data to a given type. takes in a value and a type

            Args:
                value (str): some data needed to be converted
                desired_type (str): the desired type of the given value

            Returns:
                Attempts to convert the type of 'value' to 'desired_type'. Returns value if it can,
                otherwise it returns a tuple (None, value) if desired_type is not a known type to the function or the value
                cannot be converted to the desired type.
        """

        if desired_type == "int":
            try:
                int(value)
            except ValueError:
                return None, value
            else:
                return int(value)
        elif desired_type == "float":
            try:
                float(value)
            except ValueError:
                return None, value
            else:
                return float(value)
        elif desired_type == 'tuple':
            try:
                tuple(value)
            except ValueError:
                return None, value
            else:
                return tuple(value)
        elif desired_type == 'list':
            return list(value)
        elif desired_type == 'list/int':
            return list(map(int, value.split()))
        elif desired_type == 'bool':
            try:
                bool(int(value))
            except ValueError:
                return None, value
            else:
                return bool(int(value))
        elif desired_type == 'Timestamp':
            try:
                pd.Timestamp(value)
            except ValueError:
                return None, value
            else:
                return pd.Timestamp(value)
        elif desired_type == 'Period':
            try:
                pd.Period(value)
            except ValueError:
                return None, value
            else:
                return pd.Period(value)
        elif desired_type == 'string' or desired_type == 'string/int':
            return value
        elif desired_type == 'list/string':
            return value.split()
        else:
            return None, value

    @staticmethod
    def error(list):
        """ error. Prints errors generated in the validate method.

            Args:
                list (list): a list of errors.
            TODO: put in user log
        """
        for value in list:
            if value[2] == "size":
                dLogger.error("error in " + value[0] + ", the input value: " + str(value[1])
                      + " is out of bounds. The given bounds are: (" + str("-INF" if value[3] is None else value[3])
                      + ", " + str("INF" if value[4] is None else value[4]) + ")")
            elif value[2] == "type":
                dLogger.error("error in " + value[0] + ", the input value: " + str(value[1])
                      + " is not of the correct type. The type should be: " + str(value[3]))
            elif value[2] == "type_dec":
                dLogger.error("error in " + value[0] + ", the input type: " + str(value[1])
                      + " is not of the correct type. The type should be: " + str(value[3]))
            elif value[2] == "missing":
                dLogger.error("error in " + value[0] + ", This input parameter is not in the schema")

        dLogger.info('Exiting...\n')
        print("Params error: exiting...")
        sys.exit()

    @classmethod
    def validate(cls):
        """ validates the input data. A schema file is used to validate the inputs.
            if any errors are found they are saved to a dictionary and at the end of the method it will call the error
            method to print all the input errors

             Returns: a True when complete

        """
        error_list = []

        val_root = cls.schema_tree.getroot()

        for element in list(val_root):

            attribute = None
            for key, value in cls.active_components.items():
                if element.get('name') in value:
                    attribute = key
                    break
            if attribute is None:
                continue

            attribute = cls.xmlTree.find(element.get('name'))

            for properties in list(element):

                # Check if attribute is in the schema
                try:
                    value = attribute.find(properties.get('name')).find('Value').text
                except (KeyError, AttributeError):
                    dLogger.error('Missing inputs')
                    uLogger.error(('Missing inputs. Please check CSV inputs.'))
                    error_list.append((properties.get('name'), None, "missing"))
                    continue

                obj = properties.findall("*")
                type_of_obj = obj[0].get('type')
                minimum = obj[0].get('min')
                maximum = obj[0].get('max')

                elem = cls.xmlTree.findall(element.get('name'))
                elem = elem[0].find(properties.get('name'))
                type_of_input = elem.find('Type').text
                value = Params.convert_data_type(value, type_of_input)
                tups = element.get('name'), properties.get('name')
                if tups in list(cls.sensitivity['attributes'].keys()):
                    sensitivity = attribute.find(properties.get('name')).find('Sensitivity_Parameters').text
                    for values in Params.extract_data(sensitivity, type_of_input):
                        error_list = Params.checks_for_validate(values, properties, type_of_input, type_of_obj, minimum, maximum, error_list)

                error_list = Params.checks_for_validate(value, properties, type_of_input, type_of_obj, minimum, maximum, error_list)

        # checks if error_list is not empty.
        if error_list:
            Params.error(error_list)

        return True

    def other_error_checks(self):

        scenario = self.Scenario
        storage = self.Storage
        retail_time_shift = self.retailTimeShift
        dcm = self.DCM

        start_year = scenario['start_year']
        opt_years = scenario['opt_years']
        time_series_dict = self.datasets["time_series"]
        incl_site_load = scenario['incl_site_load']

        for time_series_name in time_series_dict:
            # fix time_series index
            time_series_index = time_series_dict[time_series_name].index - pd.Timedelta('1s')

            years_included = time_series_index.year.unique()

            if any(value < years_included[0] for value in opt_years):
                dLogger.error("Error: The 'opt_years' input starts before the given Time Series.")
                return False

        if not storage:
            if storage['ch_min_rated'] > storage['ch_max_rated']:
                dLogger.error('Error: ch_max_rated < ch_min_rated. ch_max_rated should be greater than ch_min_rated')
                return False

            if storage['dis_min_rated'] > storage['dis_max_rated']:
                dLogger.error('Error: dis_max_rated < dis_min_rated. dis_max_rated should be greater than dis_min_rated')
                return False

        if scenario['end_year'].year < start_year.year:
            dLogger.error('Error: end_year < start_year. end_year should be later than start_year')
            return False

        if scenario['no_import'] and self.PV is not None:
            dLogger.error('Error: PV must be included to provide a no_import constraint')
            return False

        if scenario['no_export'] and self.PV is not None:
            dLogger.error('Error: PV must be included to provide a no_export constraint')
            return False

        # if storage['install_date'].year > scenario['opt_years'][-1]:
        #     dLogger.error('Error: install_date > opt_years. install_date should be before the last opt_years')
        #     return False

        if (dcm is not None or retail_time_shift is not None or self.PV is not None) and incl_site_load != 1:
            dLogger.error('Error: incl_site_load should be = 1')
            return False

        if (self.RA is not None or self.DR is not None) and self.Scenario['incl_sys_load'] != 1:
            dLogger.error('Error: incl_sys_load should be = 1')
            return False

        if all(start_year.year > years_included.values):
            dLogger.error('Error: "start_year" is set after the date of the time series file.')
            return False

        return True

    def prepare_technology(self):
        """ Interprets user given data and prepares it for Storage/Storage.

        Returns: collects required timeseries columns required + collects power growth rates

        """
        storage = self.active_components['storage']
        generator = self.active_components['generator']
        time_series = self.Scenario['time_series']
        # add incl flags in technology
        self.Storage.update({'binary': self.Scenario['binary'],
                             'slack': self.Scenario['slack'],
                             'dt': self.Scenario['dt']})

        if 'PV' in generator:
            self.PV.update({'rated gen': time_series.loc[:, 'PV_Gen (kW/rated kW)'],
                            'site_load': time_series.loc[:, 'Site_Load (kW)'],
                            'no_export': self.Scenario['no_export'],
                            'no_import': self.Scenario['no_import'],
                            'charge_from_solar': self.Storage['charge_from_solar']})
        if 'CAES' in storage:
            self.CAES.update({'fuel_price': self.monthly_to_timeseries(self.Scenario['monthly_data'].loc[:, ['Fuel_Price ($)']])})

        dLogger.info("Successfully prepared the Technologies")

    def prepare_services(self):
        """ Interprets user given data and prepares it for each ValueStream (dispatch and pre-dispatch).

        Returns: collects required power timeseries

        """
        dispatch_serv = self.active_components['service']
        pre_dispatch_serv = self.active_components['pre-dispatch']
        monthly_data = self.Scenario['monthly_data']
        time_series = self.Scenario['time_series']

        if 'Deferral' in pre_dispatch_serv:
            self.Deferral["last_year"] = self.Scenario["end_year"]
            self.Deferral.update({'load': time_series.loc[:, 'Deferral_Load (kW)']})

        if 'Volt' in pre_dispatch_serv:
            self.Volt['load'] = time_series.loc[:, 'Vars_Load (kW)']

        if 'RA' in pre_dispatch_serv:
            self.RA.update({'active': time_series.loc[:, 'ra_active'],
                            'value': self.monthly_to_timeseries(monthly_data.loc[:, 'RA_Price ($)']),
                            'default_growth': self.Scenario['def_growth']})

        if 'DR' in pre_dispatch_serv:
            self.DR.update({'cap_price': self.monthly_to_timeseries(monthly_data.loc[:, 'DR_Cap_Price ($)']),
                            'ene_price': self.monthly_to_timeseries(monthly_data.loc[:, 'DR_Ene_Price ($)']),
                            'dr_months': self.monthly_to_timeseries(monthly_data.loc[:, 'DR_Months']),
                            'dr_cap': self.monthly_to_timeseries(monthly_data.loc[:, 'DR_Cap (kW)']),
                            'default_growth': self.Scenario['def_growth']})

        if 'Backup' in pre_dispatch_serv:
            self.Backup.update({'price': self.monthly_to_timeseries(monthly_data.loc[:, 'Backup_Price ($)']),
                                'energy': self.monthly_to_timeseries(monthly_data.loc[:, 'Backup_Energy (kW)'])})
        if 'SR' in dispatch_serv:
            self.SR.update({'price': time_series.loc[:, 'SR_Price ($)']})

        if 'NSR' in dispatch_serv:
            self.NSR.update({'price': time_series.loc[:, 'NSR_Price ($)']})

        if 'DA' in dispatch_serv:
            self.DA.update({'price': time_series.loc[:, 'DA_Price ($)']})

        if 'FR' in dispatch_serv:
            if self.FR['CombinedMarket']:
                self.FR.update({'regu_price': np.divide(time_series.loc[:, 'FR_Price ($)'], 2),
                                'regd_price': np.divide(time_series.loc[:, 'FR_Price ($)'], 2),
                                'energy_price': time_series.loc[:, 'DA_Price ($)']})
            else:
                self.FR.update({'regu_price': time_series.loc[:, 'regu_price'],
                                'regd_price': time_series.loc[:, 'regd_price'],
                                'energy_price': time_series.loc[:, 'DA_Price ($)']})

        if 'User' in pre_dispatch_serv:
            self.User.update({'constraints': time_series.loc[:, list(np.intersect1d(sh.BOUND_NAMES, list(time_series)))]})

        if self.Scenario['customer_sided']:
            retail_prices = self.calc_retail_energy_price(self.Finance['customer_tariff'])
            if 'retailTimeShift' in dispatch_serv:
                self.retailTimeShift.update({'price': retail_prices.loc[:, 'p_energy'],
                                             'tariff': self.Finance['customer_tariff'].loc[self.Finance['customer_tariff'].Charge.apply((lambda x: x.lower())) == 'energy', :]})

            if 'DCM' in dispatch_serv:
                self.DCM.update({'tariff': self.Finance['customer_tariff'].loc[self.Finance['customer_tariff'].Charge.apply((lambda x: x.lower())) == 'demand', :],
                                 'billing_period': retail_prices.loc[:, 'billing_period']})

        dLogger.info("Successfully prepared the value-stream (services)")

    def prepare_scenario(self):
        """ Interprets user given data and prepares it for Scenario.

        TODO: add error catching and more explicit messages

        """

        # determine if mpc
        self.Scenario['mpc'] = (self.Scenario['n_control'] != 0)

        # determine if customer_sided
        self.Scenario['customer_sided'] = ('DCM' in self.active_components['service']) or ('retailTimeShift' in self.active_components['service'])

        # separate price and load timeseries data & determine data required
        # determine price timeseries columns required
        # collect power related growth rates (%/yr)
        # collect value or avoided cost related growth rates (%/yr)
        required_power_series = []
        required_price_series = []

        if self.Scenario['incl_site_load']:
            required_power_series += ['Site_Load (kW)']
        if self.Scenario['incl_aux_load']:
            required_power_series += ['Aux_Load (kW)']
        if self.Scenario['incl_sys_load']:
            required_power_series += ['System_Load (kW)']

        self.Scenario['power_timeseries'] = self.Scenario['time_series'].loc[:, required_power_series]

        self.Finance['price_timeseries'] = self.Scenario['time_series'].loc[:, required_price_series]

        dLogger.info("Successfully prepared the Scenario and some Finance")

    def prepare_finance(self):
        """ Interprets user given data and prepares it for Finance.

        """
        # include data in financial class
        self.Finance.update({'n': self.Scenario['n'],
                             'mpc': self.Scenario['mpc'],
                             'start_year': self.Scenario['start_year'],
                             'end_year': self.Scenario['end_year'],
                             'CAES': 'CAES' in self.active_components['storage'],
                             'dt': self.Scenario['dt'],
                             'opt_years': self.Scenario['opt_years'],
                             'def_growth': self.Scenario['def_growth'],
                             'frequency': self.Scenario['frequency'],
                             'verbose': self.Scenario['verbose']})
        if self.FR:
            self.Finance.update({'CombinedMarket': self.FR['CombinedMarket']})

        dLogger.info("Successfully prepared the Finance")

    def monthly_to_timeseries(self, column):
        """ Converts data given monthly into timeseries.

        Args:
            column (DataFrame):  A column of monthly data given by the user, indexed by month

        Returns: A Series of timeseries data that is equivalent to the column given

        """
        temp = pd.DataFrame(index=self.Scenario['time_series'].index)
        temp['yr_mo'] = temp.index.to_period('M')
        temp = temp.reset_index().merge(column.reset_index(), on='yr_mo', how='left').set_index(temp.index.names)
        # temp = temp.drop(columns=['yr_mo'])
        return temp[column.columns[0]]

    def preprocess_timeseries(self, time_series):
        """ Given a timeseries dataframe, checks to see if the index is timestep begining (starting at
        hour 1) or timestep ending (starting at hour 0). If the timeseries is neither, then error is
        reported to user. Transforms index into timestep beginning.

        Args:
            time_series (DataFrame): raw timeseries dataframe.

        Returns: a DataFrame with the index beginning at hour 0

        """
        # determine timeseries data frequency
        frequency = ''
        dt = self.Scenario['dt']
        if dt == 1:
            frequency = 'H'
        if dt == .5:
            frequency = '30min'
        if dt == .25:
            frequency = '15min'
        if dt == .083:
            frequency = '5min'
            self.Scenario['dt'] = 5 / 60
        if np.equal(dt, 1 / 60):
            frequency = '1min'
            self.Scenario['dt'] = 1 / 60
        self.Scenario['frequency'] = frequency

        if time_series.index.hour[0] == 1:
            # set index to denote the start of the timestamp, not the end of the timestamp
            last_day = time_series.index[-1].date()
            first_day = time_series.index[0].date()
            new_index = pd.date_range(start=first_day, end=last_day, freq=frequency, closed='left')
            time_series = time_series.sort_index()  # make sure all the time_stamps are in order
            time_series.index = new_index
            time_series.index.name = 'Start Datetime'
        elif time_series.index.hour[0] != 0:
            uLogger.error('The timeseries does not start at the begining of the day. Please start with hour as 1 or 0.')
            sys.exit()
        else:
            # set index to denote the start of the timestamp, not the end of the timestamp
            last_day = time_series.index[-1].date() + pd.Timedelta('1 day')
            first_day = time_series.index[0].date()
            new_index = pd.date_range(start=first_day, end=last_day, freq=frequency, closed='left')
            time_series = time_series.sort_index()  # make sure all the time_stamps are in order
            time_series.index = new_index
            time_series.index.name = 'Start Datetime'
        return time_series

    def calc_retail_energy_price(self, tariff):
        """ transforms tariff

        Args:
            tariff (DataFrame): raw tariff dataframe.

        Returns: a DataFrame with the index beginning at hour 0

        """
        SATURDAY = 5

        timeseries_index = self.Scenario['time_series'].index
        size = timeseries_index.size

        # Build Energy Price Vector
        temp = pd.DataFrame(index=timeseries_index)
        temp['weekday'] = (temp.index.weekday < SATURDAY).astype('int64')
        temp['he'] = (timeseries_index + pd.Timedelta('1s')).hour + 1
        # temp['hour'] = temp.index.hour
        # temp['hour fraction'] = temp.index.minute/60
        # temp['he fraction'] = temp['he'] + temp['hour fraction']
        # temp['he fraction'] = temp['hour'] + temp['hour fraction']
        temp.loc[:, 'p_energy'] = np.zeros(shape=size)

        billing_period = [[] for _ in range(size)]

        for p in range(len(tariff)):
            # edit the pricedf energy price and period values for all of the periods defined
            # in the tariff input file
            billing_num = p + 1  # billing period label that the row is labeled by in the tariff CSV
            bill = tariff.iloc[p, :]
            month_mask = (bill["Start Month"] <= temp.index.month) & (temp.index.month <= bill["End Month"])
            time_mask = (bill['Start Time'] <= temp['he']) & (temp['he'] <= bill['End Time'])
            weekday_mask = True
            exclud_mask = False
            if not bill['Weekday?'] == 2:  # if not (apply to weekends and weekdays)
                weekday_mask = bill['Weekday?'] == temp['weekday']
            if not np.isnan(bill['Excluding Start Time']) and not np.isnan(bill['Excluding End Time']):
                exclud_mask = (bill['Excluding Start Time'] <= temp['he']) & (temp['he'] <= bill['Excluding End Time'])
            mask = np.array(month_mask & time_mask & np.logical_not(exclud_mask) & weekday_mask)
            if bill['Charge'].lower() == 'energy':
                current_energy_prices = temp.loc[mask, 'p_energy'].values
                if np.any(np.greater(current_energy_prices, 0)):
                    # More than one energy price applies to the same time step
                    uLogger.warning('More than one energy price applies to the same time step.')
                # Add energy prices
                temp.loc[mask, 'p_energy'] += bill['Value']
            elif bill['Charge'].lower() == 'demand':
                for i, true_false in enumerate(mask):
                    if true_false:
                        billing_period[i].append(billing_num)
        billing_period = pd.DataFrame({'billing_period': billing_period}, dtype='object')
        temp.loc[:, 'billing_period'] = billing_period.values

        # ADD CHECK TO MAKE SURE ENERGY PRICES ARE THE SAME FOR EACH OVERLAPPING BILLING PERIOD
        # Check to see that each timestep has a period assigned to it
        if not billing_period.apply(len).all():
            dLogger.error('The billing periods in the input file do not partition the year')
            dLogger.error('please check the tariff input file')
            uLogger.error('The billing periods in the input file do not partition the year')
            uLogger.error('please check the tariff input file')
            dLogger.error('Error with tariff input file!')
            uLogger.error('Error with tariff input file!')
            sys.exit()
        if np.any(np.equal(temp.loc[:,'p_energy'].values, 0)):
            dLogger.error('The billing periods in the input file do not partition the year')
            dLogger.error('please check the tariff input file')
            uLogger.error('The billing periods in the input file do not partition the year')
            uLogger.error('please check the tariff input file')
            dLogger.error('Error with tariff input file!')
            uLogger.error('Error with tariff input file!')
            sys.exit()
        return temp

    # TODO: consider ways to validate special Allowed Values for like loc, valid file path name, opt_years, n, pv_loc, ulsoc, llsoc - TN
    @staticmethod
    def checks_for_validate(value, properties, type_of_input, type_of_obj, minimum, maximum, error_list):
        """ Helper function to validate method. This runs the checks to validate the input data.

            Args:
                value ():
                properties ():
                type_of_input ():
                type_of_obj ():
                minimum (int or float):
                maximum (int or float):
                error_list (list):

            Returns: error_list (list):

        """
        # check if type of schema matches type in input file
        if type_of_obj != type_of_input:
            error_list.append((properties.get('name'), type_of_input, "type_dec", type_of_obj))
            return error_list

        # check if there was an error converting the given data in the convert_data_type method
        if type(value) == tuple and value[0] is None:
            error_list.append((properties.get('name'), value[1], "type", type_of_obj))
            return error_list

        # TODO: this is hackey (see TN's comment)
        # skip list, period, and string inputs before the range test
        if type_of_obj == 'list' or type_of_obj == 'Period' or type_of_obj == 'string' or type_of_obj == "string/int":
            return error_list

        # check if data is in valid range
        if minimum is None and maximum is None:
            return error_list
        elif maximum is None and minimum is not None:
            minimum = Params.convert_data_type(minimum, type_of_obj)
        elif maximum is not None and minimum is None:
            maximum = Params.convert_data_type(maximum, type_of_obj)
        else:
            minimum = Params.convert_data_type(minimum, type_of_obj)
            maximum = Params.convert_data_type(maximum, type_of_obj)

        if minimum is not None and value < minimum:
            error_list.append((properties.get('name'), value, "size", minimum, maximum))
            return error_list

        if maximum is not None and value > maximum:
            error_list.append((properties.get('name'), value, "size", minimum, maximum))
            return error_list

        return error_list

    def load_data_sets(self):
        """Saves data to Scenario dictionary."""

        self.Scenario["time_series"] = self.preprocess_timeseries(self.datasets["time_series"][self.Scenario["time_series_filename"]])
        self.Scenario["monthly_data"] = self.datasets["monthly_data"][self.Scenario["monthly_data_filename"]]
        self.Finance["customer_tariff"] = self.datasets["customer_tariff"][self.Scenario["customer_tariff_filename"]]
        self.Scenario["cycle_life"] = self.datasets["cycle_life"][self.Scenario["cycle_life_filename"]]

        dLogger.info("Data sets are loaded successfully.")

    @staticmethod
    def search_schema_type(root, component_name):
        """
        Function to look for the type of the component
        Ex: storage-type, storage-technology, pre-dispatch, services, generator, finance, or scenario

        Args:
            root: root of the XML tree
            component_name: name of the component to be determined for the type

        Returns:

        """
        for child in root:
            attributes = child.attrib
            if attributes.get('name') == component_name:
                if attributes.get('type') is None:
                    return "other"
                else:
                    return attributes.get('type')

    @classmethod
    def class_summary(cls):
        """
            Function to summarize the Model_Parameters_Template for previsualization

            Return: the summary table
        """
        tree = cls.xmlTree
        treeRoot = tree.getroot()
        schema = cls.schema_tree

        dLogger.info("Printing summary table for class Params")
        table = PrettyTable()
        table.field_names = ["Category", "Tag", "Active?", "Key", "Analysis?",
                             "Value", "Value Type", "Sensitivity"]
        for component in treeRoot:
            schemaType = cls.search_schema_type(schema.getroot(), component.tag)
            activeness = component.attrib.get('active')
            if len(component) > 0:
                for property in component:
                    table.add_row([schemaType, component.tag, activeness, property.tag, property.attrib.get('analysis'),
                        property.find('Value').text, property.find('Type').text, property.find('Sensitivity_Parameters').text])
            else:
                table.add_row([schemaType, component.tag, activeness, '', '', '', '', ''])

        uLogger.info('Params class summary: \n' + str(table))

        print(table)

        return table

    @classmethod
    def sens_summary(cls):
        """
             Function to summarize the data frame for Sensitivity Analysis from Params Class
             Including the component names, its data variables, and all the possible data combinations of Sensitivity

             Return: the summary table
         """

        df = cls.df_analysis
        dLogger.info("Printing sensitivity summary table for class Params")

        if len(df) == 0:
            return 0
        else:
            headers = ["Scenario"]
            for v in df.columns.values:
                headers.append(v)
            table2 = PrettyTable()
            table2.field_names = headers

            for index, row in df.iterrows():
                entry = []
                entry.append(index)
                for col in row:
                    entry.append(str(col))
                table2.add_row(entry)

            uLogger.info("Sensitivity summary: \n" + str(table2))
            return table2

    @classmethod
    def series_summary(cls):
        """
            Function to plot all the time series data provided from the Input template file
        """
        time_series_dict = cls.datasets["time_series"]
        for ts in time_series_dict:
            fig = plt.figure('EPRI', figsize=(20, 20), dpi=80, facecolor='w', edgecolor='k')

            # this method has trouble reading files that are not properly csv formatted - TN
            series = read_csv(ts, encoding="utf-8", header=0, parse_dates=[0], index_col=0, squeeze=True)

            x = series.iloc[1:, 0].values
            labels = list(series.columns.values)
            priceindex = []
            loadindex = []
            prices = []
            loads = []
            for column in range(len(labels)-1):
                if "price" in labels[column+1] or "Price" in labels[column+1]:
                    prices.append(series.iloc[1:, column+1].values)
                    priceindex.append(column+1)
                else:
                    loads.append(series.iloc[1:, column+1].values)
                    loadindex.append(column+1)

            ax1 = fig.add_subplot(211)
            ax1.set_title('Time Series Data for ' + ts)
            for i in range(len(prices)):
                ax1.plot(x, prices[i], label=labels[priceindex[i]])
            ax1.set_xlabel("DateTime")
            ax1.set_ylabel("Prices")
            box = ax1.get_position()
            ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            ax2 = fig.add_subplot(212)
            for i in range(len(loads)):
                ax2.plot(x, loads[i], label=labels[loadindex[i]])
            ax2.set_xlabel("DateTime")
            ax2.set_ylabel("Loads")
            box = ax2.get_position()
            ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            # uncomment this if you want to plot the time series data without using GUI - TN
            # plt.show()
            # plt.close(fig)

        return fig

    @classmethod
    def table_summary(cls):
        """
            Function to summarize the other non time-series data in a tabular format.
            Ex: battery cycle life, tariff data, monthly data, etc

            Return: the summary table
        """

        monthly_data_dict = cls.datasets["monthly_data"]
        for md in monthly_data_dict:
            dLogger.info("Building plots for Monthly Data: %s", md)
            data = pd.read_csv(md, sep=',', header=None)
            labels = data.iloc[0, :].values
            table = PrettyTable()
            for column in range(len(labels)):
                li = data.loc[1:len(data.index), column].values
                table.add_column(labels[column], li)
            uLogger.info("Monthly Data table: \n" + str(table))
            dLogger.info("Finished making table for Monthly Data...")

        customer_tariff_dict = cls.datasets["customer_tariff"]
        for ct in customer_tariff_dict:
            dLogger.info("Building plots for Customer Tariff Data: %s", ct)
            data = pd.read_csv(ct, sep=',', header=None)
            labels = data.iloc[0, :].values
            table = PrettyTable()
            for column in range(len(labels)):
                li = data.loc[1:len(data.index), column].values
                table.add_column(labels[column], li)
            uLogger.info("Customer Tariff table: \n" + str(table))
            dLogger.info("Finished making table for Customer Tariff...")

        cycle_life_dict = cls.datasets["cycle_life"]
        for cl in cycle_life_dict:
            dLogger.info("Building plots for Battery Cycle Life Data: %s", cl)
            data = pd.read_csv(cl, sep=',', header=None)
            labels = data.iloc[0, :].values
            table = PrettyTable()
            for column in range(len(labels)):
                li = data.loc[1:len(data.index), column].values
                table.add_column(labels[column], li)
            uLogger.info("Battery Cycle Life table: \n" + str(table))
            dLogger.info("Finished making table for Battery Cycle Life...")

        return table

    def instance_summary(self):
        """
        Prints a summary of input data for a specific scenario/instance

        Returns: 0 when successfully complete
        """
        uLogger.info("Summary for an instance of sensitivity:")
        for key, value in self.active_components.items():
            uLogger.info("CATEGORY: %s", key)
            for v in value:
                element = getattr(self, v)
                c = copy.copy(element)
                c.pop('cycle_life', None)
                c.pop('monthly_data', None)
                c.pop('customer_tariff', None)
                c.pop('time_series', None)
                uLogger.info("Element %s: %s", v, str(c))
        dLogger.info("Finished making instance summary for sensitivity.")

        return 0
