"""
SVETapp.py

This Python script serves as the GUI launch point executing the Python-based version of StorageVET
(AKA StorageVET 2.0 or SVETpy).
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2019. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani',
               'Micah Botkin-Levy', "Thien Nguyen", 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import sys, traceback, time, logging, os
from pathlib import Path
import GUI

from datetime import datetime
from pathlib import Path
from Params import Params
from run_StorageVET import run_StorageVET
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# from GUI import Ui_MainWindow
# from PySide2.QtWidgets import QMainWindow, QApplication, QAction, QFileDialog, QLabel, QMessageBox, \
#     QSpacerItem, QSizePolicy, QGridLayout, QWidget, QScrollArea, QVBoxLayout, QHBoxLayout, QPushButton, \
#     QCheckBox, QSplashScreen, QProgressBar
# from PySide2.QtCore import Qt, QDir, QObject, SIGNAL, SLOT, QRunnable, QThreadPool, QThread, QTimer
# from PySide2.QtGui import QPalette, QIcon, QPixmap

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

# PySide2 supports Python 3.5 and above
# PySide2 (QT for Python) can be used for open-source commercial software, while PyQT is more restricted

# TODO: eventually will merge setuptools, setupmenu, setupbuttons, and part of constructor into setupUi
#  and move that into GUI class - TN

path = Path('./logs')
try:
    os.mkdir(path)
except OSError:
    print("Creation of the directory %s failed. Possibly already created." % path)
else:
    print("Successfully created the directory %s " % path)

LOG_FILENAME = path.joinpath(f"gui_log_{datetime.now().strftime('%H_%M_%S_%m_%d_%Y.log')}")

handler = logging.FileHandler(Path(LOG_FILENAME))
handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
gLogger = logging.getLogger('Gui')
gLogger.setLevel(logging.DEBUG)
gLogger.addHandler(handler)
gLogger.info('Started logging...')

# TODO: Multi-Threading is slow for test cases that involve Sensitivity Analysis, we will consider Multi-Processes - TN

class Worker(QRunnable):
    """
        Worker thread

        Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

        :param callback: The function callback to run on this worker thread. Supplied args and
                         kwargs will be passed through to the runner.
        :type callback: function
        :param args: Arguments to pass to the callback function
        :param kwargs: Keywords to pass to the callback function
    """

    def __init__(self, params):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.input = params

    def run(self):
        """
            Initialise the runner function with passed args, kwargs.
        """
        start = time.time()
        run_StorageVET(self.input)
        end = time.time()
        statement = "Simulation thread completed within " + str(end - start) + " sec. \nIt successfully ran."
        print(statement)
        gLogger.info(statement)

#class SVETapp(QMainWindow, Ui_MainWindow):
class SVETapp(QMainWindow):
    """
        Provide the GUI for StorageVET to run as a Python-based application, instead of via Terminal console.
        QMainWindow is chosen because it has its own window management, and its own layout that includes QToolBars,
        QDockWidgets, a QMenuBar, and a QStatusBar. It has a separate Central Widget which contains the main application
        content and can have any kind of Widget.

        Args:
            QMainWindow (QWidget object): main application window for StorageVET
    """

    gLogger.info('Started configuring SVETapp...')

    def __init__(self, parent=None):
        """
            Constructor for the QMainWindow GUI, creating and customizing the basic features of the GUI
            setupUi will eventually include setupButtons, setupTools, setupMenu, setupDWidget, and setupStatus
            QMainWindow has 5 main components for its layout:
            Menu Bar
            Toolbars
            Dock Widgets
            Status Bar
            Central Widget
        """
        super(SVETapp, self).__init__(parent)
        # self.setupUi(self)

        self.setWindowTitle("StorageVET")
        self.setGeometry(50, 50, 700, 400)
        self.setWindowIcon(QIcon("Images/epri_logo.png"))

        checkBox = QCheckBox('Enlarge Window', self)
        checkBox.move(100, 200)
        checkBox.stateChanged.connect(self.enlargeWindow)

        self.dockWidget = QDockWidget("Dock")
        self.dockWidget.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.dockWidget.setWidget(checkBox)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.dockWidget)

        self.stackedWidgets = QStackedWidget()

        self.label = QPlainTextEdit("                                            Welcome to StorageVET \n \n" +
            "        The Electric Power Research Institute's energy storage system \n" +
            "        analysis, dispatch, modelling, optimization, and valuation tool. \n" +
            "        Should be used with Python 3.6, Pandas 0.24, and CVXPY 1.0.21 \n" +
            "        Copyright 2019. Electric Power Research Institute (EPRI). \n        All Rights Reserved.")
        self.text = QTextEdit()

        figure = Figure()
        self.canvas = FigureCanvas(figure)

        self.stackedWidgets.addWidget(self.label)
        self.stackedWidgets.addWidget(self.text)

        #self.text.setAlignment(Qt.AlignCenter)
        self.scrollArea = QScrollArea(widgetResizable=True)
        self.scrollArea.setWidget(self.stackedWidgets)
        self.setCentralWidget(self.scrollArea)

        self.inputFileName = ""
        self.init = ""

        self.msgBox = QMessageBox()
        self.msgBox.setWindowTitle('EPRI Message')
        self.msgBox.setWindowIcon(QIcon("Images/epri_logo.png"))

        # self.statusBar = QStatusBar()
        # self.statusBar.showMessage("Ready for input")

        self.setupmenu()
        self.setupbuttons()
        self.setuptools()

        self.threadpool = QThreadPool()

        gLogger.info("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

        # self.runProgress = QProgressBar(self)
        # self.runProgress.setGeometry(200, 80, 250, 20)

        # just in case we run SVETapp from a Python standalone environment, make sure to locate its new directory
        self.current_dir = QDir.currentPath()
        if "storagevet_pkg" not in self.current_dir:
            self.current_dir = self.current_dir + "/Lib/site-packages/storagevet_pkg"

        gLogger.info('Finished configuration for SVETapp GUI window...')

    def setupbuttons(self):
        """
            Set up the buttons needed for the main GUI Window
        """

        btn = QPushButton("Run", self)
        btn.resize(btn.minimumSizeHint())
        btn.move(15,250)
        btn.clicked.connect(self.run)

        # self.showButton.setText("Run")
        # self.connect(self.showButton, SIGNAL("clicked()"), self.run)

    def setuptools(self):
        """
            Set up all the features on the Tool Bar for the main GUI window
        """

        extractAction = QAction(QIcon("Images/quit.png"), 'Sudden Quit', self)
        extractAction.triggered.connect(self.close)
        self.toolbar = self.addToolBar("Sudden Quit")
        self.toolbar.addAction(extractAction)

    def enlargeWindow(self, state):
        """
            Optional feature to enlarge the main GUI window
            Part of the Dock Widgets
        """

        if state == Qt.Checked:
            self.setGeometry(50, 50, 1000, 600)
        else:
            self.setGeometry(50, 50, 500, 300)

    def setupmenu(self):
        """
            Set up all the subMenus and functions on the Menu Bar
        """

        menubar = self.menuBar()
        filemenu = menubar.addMenu("File")
        initmenu = menubar.addMenu("Initialization")
        premenu = menubar.addMenu("Pre-visualization")
        runmenu = menubar.addMenu("Run")

        open = QAction("Upload File",self)
        open.setShortcut("Ctrl+U")
        open.setStatusTip('Upload the Input File')
        open.triggered.connect(self.openfile)
        filemenu.addAction(open)
        quit = QAction("Quit", self)
        quit.setShortcut("Ctrl+Q")
        quit.setStatusTip('Leave the App')
        quit.triggered.connect(self.close)
        filemenu.addAction(quit)

        init = QAction("Initialize Inputs", self)
        init.triggered.connect(self.initialize)
        initmenu.addAction(init)
        validate = QAction("Validate Inputs", self)
        validate.triggered.connect(self.validate)
        initmenu.addAction(validate)

        inputsummary = QAction("Input Summary", self)
        inputsummary.triggered.connect(self.summary)
        premenu.addAction(inputsummary)
        sens_summary = QAction("Sensitivity Summary", self)
        sens_summary.triggered.connect(self.sens_summary)
        premenu.addAction(sens_summary)
        tablesummary = QAction("Table Summary", self)
        tablesummary.triggered.connect(self.tablesummary)
        premenu.addAction(tablesummary)
        seriessummary = QAction("Time Series Summary", self)
        seriessummary.triggered.connect(self.seriessummary)
        premenu.addAction(seriessummary)

        run = QAction("Run StorageVET", self)
        run.setShortcut("Ctrl+R")
        run.setStatusTip('Run the App')
        run.triggered.connect(self.run)
        runmenu.addAction(run)

    def openfile(self):
        """
            Function to open the folder directory to look and select the input files
            Part of the MenuBar
        """

        self.stackedWidgets.setCurrentIndex(0)
        filename, _ = QFileDialog.getOpenFileName(self, "Upload File", self.current_dir,
                                "CSV files (*.csv);;Images (*.png *.xpm *.jpg);;Text files (*.txt);;XML files (*.xml)")

        if filename != "":
            # infile = open(filename, "r")
            # contents = infile.read()
            # infile.close()
            # self.text.setText(contents)
            self.inputFileName = filename
            self.msgBox.setText('The provided file is accepted')
            self.msgBox.show()
            #self.msgBox.exec_()
            gLogger.info('User configured data with the provided file: %s', self.inputFileName)
        else:
            self.msgBox.setText('No file or invalid file was provided')
            self.msgBox.exec_()
            gLogger.info('User did not provide any file.')

    def initialize(self):
        """
            Function to call the initialize function on the Params object of the StorageVET application
            Part of the MenuBar
        """

        self.stackedWidgets.setCurrentIndex(0)
        if self.inputFileName:
            if self.inputFileName.endswith(".csv"):
                self.inputFileName = Params.csv_to_xml(self.inputFileName)
            # Initialize the Input Object from Model Parameters and Simulation Cases
            Params.initialize(self.inputFileName, ".\storagevet\Schema.xml")
            self.init = 'Yes'
            self.msgBox.setText('Successfully initialized the Params class with the XML file.')
            self.msgBox.exec_()
            gLogger.info('User successfully initialized the Params class with the XML file.')
        else:
            self.msgBox.setText('Params has not been initialized to validate.')
            self.msgBox.exec_()
            gLogger.info('User has not given an input file to initialize.')

    def validate(self):
        """
            Function to call the validate function on the Params object after Initialization
            Part of the MenuBar
        """

        self.stackedWidgets.setCurrentIndex(0)
        if self.init:
            Params.validate()
            self.msgBox.setText('Successfully validated the Params class with the XML file.')
            self.msgBox.exec_()
            gLogger.info('User successfully validated the Params class with the XML file.')
        else:
            self.msgBox.setText('Input has not been initialized to validate.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to validate.')

    def summary(self):
        """
            Function to call the class_summary function on the Params object
            Part of the MenuBar
        """

        if self.init:
            table = Params.class_summary()
            table.align['Sensitivity'] = "l"
            table.align['Value'] = "l"
            self.text.setText(table.get_html_string())
            self.stackedWidgets.setCurrentIndex(1)
            gLogger.info('User successfully finished class summary.')
        else:
            self.msgBox.setText('Input has not been initialized to summarize.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to summarize.')

    def seriessummary(self):
        """
            Function to call the series_summary function on the Params object
            Part of the MenuBar
        """

        if self.init:
            self.canvas = FigureCanvas(Params.series_summary())
            self.canvas.draw()
            self.stackedWidgets.addWidget(self.canvas)
            self.stackedWidgets.setCurrentIndex(2)

            self.msgBox.setText('Successfully plotted all the provided time series.')
            self.msgBox.exec_()
            gLogger.info('User successfully plotted all the provided time series.')
        else:
            self.msgBox.setText('Input has not been initialized to plot time series.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to plot time series..')

    def tablesummary(self):
        """
            Function to call the table_summary function on the Params object
            Part of the MenuBar
        """

        if self.init:
            table = Params.table_summary()
            table.align['Cycle Depth Upper Limit'] = "r"
            table.align['Cycle Life Value'] = "r"
            self.text.setText(table.get_html_string())
            self.stackedWidgets.setCurrentIndex(1)
            self.msgBox.setText('Successfully printed the tabular data.')
            self.msgBox.exec_()
            gLogger.info('User successfully printed the tabular data.')
        else:
            self.msgBox.setText('Input has not been initialized to summarize the cycle life table.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to summarize the cycle life table.')

    def sens_summary(self):
        """
            Function to call the sens_summary function on the Params object
            Part of the MenuBar
        """

        if self.init:
            table = Params.sens_summary()
            if table == 0:
                self.stackedWidgets.setCurrentIndex(0)
                self.msgBox.setText('There is no sensitivity data.')
                self.msgBox.exec_()
                gLogger.info('File has no sensitivity data to summarize.')
            else:
                self.text.setText(table.get_html_string())
                self.stackedWidgets.setCurrentIndex(1)
                self.msgBox.setText('Successfully printed the sensitivity data.')
                self.msgBox.exec_()
                gLogger.info('User successfully printed the sensitivity data.')
        else:
            self.msgBox.setText('Input has not been initialized to summarize.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to summarize.')

    def run(self):
        """
            Function to call the class and run the simulation run_StorageVET
            Part of the MenuBar
        """
        self.stackedWidgets.setCurrentIndex(0)
        if self.init:
            self.text.setText("Simulation in progress.")
            self.stackedWidgets.setCurrentIndex(1)
            gLogger.info("User clicked Run the Application")
            # run_StorageVET(Params)
            worker = Worker(Params)
            print("Simulation thread started.")
            self.threadpool.start(worker)
        else:
            self.msgBox.setText('Input has not been initialized to run.')
            self.msgBox.exec_()
            gLogger.info('User has not initialized the input to run the simulation.')

if __name__ == "__main__":
    """
        the Main section for the GUI Window to start
    """

    app = QApplication(sys.argv)
    mainWindow = SVETapp()

    pixmap = QPixmap("Images/splashscreen.png")
    splash = QSplashScreen(pixmap)
    splash.show()
    acknowledgement = QMessageBox.question(mainWindow, 'Pre-authorization', "You must read and accept the "
                                        "acknowledgement to run this application.",  QMessageBox.Yes | QMessageBox.No)

    # need this for event handlers and threading
    # app.processEvents()

    if acknowledgement == QMessageBox.Yes:
        gLogger.info("SVETapp GUI window started...")
        mainWindow.show()
        splash.finish(mainWindow)
        status = app.exec_()
    else:
        mainWindow.close()
        splash.close()
        status = app.exit()

    gLogger.info("SVETapp GUI window exited...")
    sys.exit(status)





