"""
VoltVar.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import Constraint as Const
import numpy as np
import sys, logging
import Library as sh

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class VoltVar(ValueStream):
    """ Reactive power support, voltage control, power quality. Each service will be daughters of the PreDispService class.
    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)
        """
        # generate the generic service object
        ValueStream.__init__(self, tech, 'VoltVAR', dt)

        # add voltage support specific attributes
        self.vars_load = params['load']
        self.inv_max = params['inv_max']
        self.p_volt = params['price']
        # TODO: add tech related rte here or maybe we should start passing in technologies (DERVET)

        if self.inv_max < max(self.vars_load):
            dLogger.error('given inverter maximum is less than committed vars support in VoltVar')
            sys.exit()

        # prevent inverter import overload
        ch_max_rated = self.storage.physical_constraints['ch_max_rated'].value
        ch_max_vals = ch_max_rated-(load_data.dc_gen + np.sqrt(np.square(np.ones(len(self.vars_load))*self.inv_max) - np.square(self.vars_load)))
        ch_max_sub = Const.Constraint('ch_max_sub', self.name, ch_max_vals.clip(lower=0))

        dis_min_vals = -load_data.dc_gen - np.sqrt(np.square(np.ones(len(self.vars_load))*self.inv_max) - np.square(self.vars_load))
        dis_min_add = Const.Constraint('dis_min_add', self.name, dis_min_vals.clip(lower=0))

        # prevent inverter export overload
        ch_min_vals = load_data.dc_gen - np.sqrt(np.square(np.ones(len(self.vars_load))*self.inv_max) - np.square(self.vars_load))
        ch_min_add = Const.Constraint('ch_min_add', self.name, ch_min_vals.clip(lower=0))

        dis_max_rated = self.storage.physical_constraints['dis_max_rated'].value
        dis_max_vals = dis_max_rated-(-load_data.dc_gen + np.sqrt(np.square(np.ones(len(self.vars_load))*self.inv_max) - np.square(self.vars_load)))
        dis_max_sub = Const.Constraint('dis_max_sub', self.name, dis_max_vals.clip(lower=0))

        self.constraints = {'ch_max_sub': ch_max_sub,
                            'dis_min_add': dis_min_add,
                            'ch_min_add': ch_min_add,
                            'dis_max_sub': dis_max_sub}

        # add financials
        load_data['p_volt'] = self.p_volt
        time_series = sh.disagg_col(load_data, ['year', 'opt_agg'], 'p_volt')
        self.obj_val = -time_series.groupby('opt_agg')['p_volt_disagg'].sum()
