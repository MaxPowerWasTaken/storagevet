"""
ResourceAdequacy.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import Constraint as Const
import pandas as pd
import copy, logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class ResourceAdequacy(ValueStream):
    """ Resource Adequacy ValueStream. Each service will be daughters of the PreDispService class.
    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)
        """

        # generate the generic service object
        ValueStream.__init__(self, tech, 'ResourceAdequacy', dt)

        # # generate the generic predispatch service object
        # PreDispService.PreDispService.__init__(self, 'ResourceAdequacy', storage)

        # add RA specifici attributes
        self.ra_events = params['events']
        self.ra_idmode = params['ra_idmode']
        self.ra_dispmode = params['dispmode']
        self.ra_length = params['length']
        self.ra_maxp = params['maxp']
        self.ra_maxe = params['maxe']
        self.dt = params['dt']

        time_series = copy.deepcopy(load_data)

        if self.ra_idmode == 'Peak by Year':
            # find ra_events number of events in year where system_load is at peak
            max_int = time_series.sort_values(by='system_load', ascending=False)
            max_days = max_int.drop_duplicates(subset=['date'])
            self.peak_intervals = max_days.index[0:self.ra_events].values
        elif self.ra_idmode == 'Peak by Month':
            # find ra_events number of events in month where system_load is at peak
            max_int = time_series.sort_values(by=['yr_mo', 'system_load'], ascending=False)
            max_days = max_int.drop_duplicates(subset=['date'])
            self.peak_intervals = max_days.groupby('yr_mo').head(self.ra_events).index.values
        elif self.ra_idmode == 'Peak by Month with Active Hours':
            # find ra_events number of events in month where system_load is at peak during active hours
            max_int = time_series[time_series.ra_active == 1].sort_values(by=['yr_mo', 'system_load'],ascending=False)
            max_days = max_int.drop_duplicates(subset=['date'])
            self.peak_intervals = max_days.groupby('yr_mo').head(self.ra_events).index.values

        # logic to find intervals of RA event
        time_series['event_interval'] = 0
        # odd intervals straddle peak
        # even intervals have extra interval after peak
        precount = max((self.ra_length/self.dt - 1) // 2, 0)
        postcount = max((self.ra_length/self.dt - 1) // 2 + 1, 0)
        for i in self.peak_intervals:
            first_int = i-pd.Timedelta(precount*self.dt, unit='h')
            last_int = i+pd.Timedelta(postcount * self.dt, unit='h')
            if first_int < time_series.index[0] or last_int > time_series.index[-1]:
                # TODO what if interval is first or last interval
                dLogger.error('not built out ')
                quit()  # TODO: no.
            event_range = pd.date_range(start=first_int,
                                        end=last_int,
                                        periods=self.ra_length/self.dt)
            time_series.loc[event_range, 'event_interval'] = 1
        self.event_intervals = time_series[time_series.event_interval == 1].index

        # if self.ra_dispmode assume RA dispatch create discharge constraint else create energy constraint
        disp_power = min(self.ra_maxe * params['ene_max_rated'] / self.ra_length, self.ra_maxp * params['dis_max_rated'])
        if self.ra_dispmode:
            # create dispatch power constraint
            time_series['dis_min'] = 0
            time_series.loc[(time_series.event_interval == 1), 'dis_min'] = disp_power
            dis_min_add = Const.Constraint('dis_min_add', self.name, time_series['dis_min'])
            self.constraints = {'dis_min_add': dis_min_add}
        else:
            # create energy reservation constraint
            time_series['ene_min'] = 0
            disp_energy = min(self.ra_maxe*params['ene_max_rated'], self.ra_maxp*params['dis_max_rated']*self.ra_length)
            time_series.loc[(time_series.event_interval == 1), 'ene_min'] = disp_energy
            ene_min_add = Const.Constraint('ene_min_add', self.name, time_series['ene_min'])
            self.constraints = {'ene_min_add': ene_min_add}

        # # add financials
        # # assumes that opt_agg is less or equal to month
        # time_series = pd.merge(time_series, pd.DataFrame(fin_data[['yr_mo', 'opt_agg', 'p_ra']]), on=['yr_mo', 'opt_agg'], left_index=True, right_index=True)
        # time_series['ra_pay'] = time_series['p_ra']*disp_power
        # time_series = lib.disagg_col(time_series, ['yr_mo', 'opt_agg'], 'ra_pay')
        # self.obj_val = -time_series.groupby('opt_agg')['ra_pay_disagg'].sum()
        # self.mean_inputs = time_series[['opt_agg', 'ra_active', 'p_ra']].groupby('opt_agg').mean()
