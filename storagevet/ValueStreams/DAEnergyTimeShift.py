"""
DAEnergyTimeShift.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import numpy as np
import cvxpy as cvx
import pandas as pd
import Library as sh
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class DAEnergyTimeShift(ValueStream):
    """ Day-Ahead Energy Time Shift. Each service will be daughters of the ValueStream class.

    """

    def __init__(self, params, tech, dt):
        """ Generates the objective function, finds and creates constraints.

        Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            dt (float): optimization timestep (hours)
        """
        ValueStream.__init__(self, tech, 'DA', dt)
        self.price = params['price']
        self.growth = params['growth']  # growth rate of energy prices (%/yr)

    def objective_function(self, variables, subs):
        """ Generates the full objective function, including the optimization variables.

        Args:
            variables (Dict): Dictionary of optimization variables
            subs (DataFrame): Subset of time_series data that is being optimized
            mask (DataFrame): DataFrame of booleans used, the same length as self.time_series. The value is true if the
                        corresponding column in self.time_series is included in the data to be optimized.

        Returns:
            The expression of the objective function that it affects. This can be passed into the cvxpy solver.

        """

        generation_array = np.array(subs.loc[:, "generation"])
        load_array = np.array(subs.loc[:, "load"])

        size = subs.index.size
        load = cvx.Parameter(size, value=load_array, name='load')
        gen = cvx.Parameter(size, value=generation_array, name='gen')
        p_da = cvx.Parameter(size, value=self.price.loc[subs.index].values, name='da price')
        dt = cvx.Parameter(value=self.dt, name='dt')
        temp_exp = cvx.sum(p_da*dt*load-p_da*dt*variables['dis'] + p_da*dt*variables['ch'] - p_da*dt*gen)
        self.costs.append(temp_exp)
        return {self.name: temp_exp}

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        data_year = self.price.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                source_data = self.price[self.price.index.year == source_year.year]  # use source year data
                new_data = sh.apply_growth(source_data, self.growth, source_year, yr, frequency)
                self.price = pd.concat([self.price, new_data], sort=True)  # add to existing
