"""
Deferral.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import numpy as np
import cvxpy as cvx
from ValueStreams.ValueStream import ValueStream
import pandas as pd
import Library as Lib
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class Deferral(ValueStream):
    """ Investment deferral. Each service will be daughters of the PreDispService class.

    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)
        """

        # generate the generic predispatch service object
        ValueStream.__init__(self, tech, 'Deferral', dt)

        # add Deferral specific attributes
        self.deferral_max_import = params['max_import']  # positive
        self.deferral_max_export = params['max_export']  # negative
        self.last_year = params['last_year'].year
        self.min_power_requirement = 0
        self.min_energy_requirement = 0
        self.load = params['load']  # deferral load
        self.growth = params['growth']  # Growth Rate of deferral load (%/yr)

        # self.load_tot = params['deferral_load'] + load_data['load']
        # self.gen_tot = load_data['deferral_gen'] + load_data['ac_gen'] + load_data['dc_gen']

        # The following is not included within the optimization (place holder for future development)
        # add financials
        # load_data['deferral_price'] = self.p_deferral
        # load_data = lib.disagg_col(load_data, ['year', 'opt_agg'], 'deferral_price')
        #
        # self.obj_val = pd.Series(0, index=load_data.opt_agg.unique())
        # self.obj_val.update(-load_data.loc[:].groupby('opt_agg')['deferral_price_disagg'].sum())
        # self.obj_val.name = 'deferral_price'

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        data_year = self.load.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                source_data = self.load[self.load.index.year == source_year.year]  # use source year data
                new_data = Lib.apply_growth(source_data, self.growth, source_year, yr, frequency)
                self.load = pd.concat([self.load, new_data], sort=True)  # add to existing

    def build_constraints(self, opt_vars, subs):
        """Default build constraint list method. Used by services that do not have constraints.

        Args:
            opt_vars (Dict): dictionary of variables being optimized
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            An list of constraints to be included for deferral
        """
        # optimization variables
        dis = opt_vars['dis']
        ch = opt_vars['ch']

        # adding constraints to ensure power dispatch does not violate thermal limits of transformer deferred
        # only include them if deferral is not going to fail
        constraints = []
        if subs.index.year[-1] > self.last_year:
            gen_tot = self.load.loc[subs.index] + subs['load']
            load_tot = subs['generation']

            constraints += [cvx.NonPos(ch - dis - gen_tot + load_tot + self.deferral_max_export)]
            constraints += [cvx.NonPos(-ch + dis - gen_tot + load_tot - self.deferral_max_import)]

        return constraints

    def precheck_failure(self, tstep, rte, sys_load, generation):
        """
        This function takes in a vector of storage power requirements (negative=charging and positive=discharging) [=] kW
        that are required to perform the deferral as well as a time step (tstep) [=] hrs

        Args:
            tstep (float): timestep of the data in hours
            rte (float): round trip efficiency of storage
            sys_load (Series): Series or DataFrame of total load
            generation (Series): Series or DataFrame of total generation

        Returns:
            A list of minimum storage power (kw_rated) and energy (kwh_min) capacity required to meet deferral
        """
        load = self.load.values + sys_load.values
        net_feeder_load = load - generation.values

        # Determine power requirement of the storage:
        # (1) anytime the net_feeder_load goes above deferral_max_import
        positive_feeder_load = net_feeder_load.clip(min=0)
        positive_load_power_req = positive_feeder_load - self.deferral_max_import
        positive_power_req = positive_load_power_req.clip(min=0)
        # (2) anytime the net_feeder_load goes below deferral_max_exports (assumes deferral_max_export < 0)
        negative_feeder_load = net_feeder_load.clip(max=0)
        negative_load_power_req = negative_feeder_load - self.deferral_max_export
        negative_power_req = negative_load_power_req.clip(max=0)
        # The sum of (1) and (2)
        sto_p_req = positive_power_req + abs(negative_power_req)

        # Loop through time steps. If the storage is forced to dispatch from the constraint,
        # return to nominal SOC as soon as possible after.
        kw_rated = max(abs(sto_p_req))
        sto_dispatch = np.zeros(sto_p_req.shape)
        e_walk = np.zeros(sto_p_req.shape)  # how much the energy in the ESS needs to wander #Definitely not a star wars pun
        for step in range(len(sto_p_req)):
            if step == 0:
                e_walk[step] = -tstep * sto_p_req[0]  # initialize at nominal SOC
                sto_dispatch[step] = sto_p_req[0]  # ignore constaints imposed by the first timestep of the year
            elif sto_p_req[step] > 0:  # if it is required to dispatch, do it
                sto_dispatch[step] = sto_p_req[step]
                e_walk[step] = e_walk[step - 1] - sto_dispatch[step] * tstep  # kWh
            elif sto_p_req[step] < 0:
                sto_dispatch[step] = sto_p_req[step]
                e_walk[step] = e_walk[step - 1] - sto_dispatch[step] * tstep * rte
            elif e_walk[step - 1] < 0:  # Otherwise contribute its full power to returning energy to nominal
                sto_dispatch[step] = -min(abs(kw_rated), abs(e_walk[step - 1] / tstep))
                e_walk[step] = e_walk[step - 1] - sto_dispatch[step] * tstep * rte  # kWh
            elif e_walk[step - 1] > 0:
                sto_dispatch[step] = min(abs(kw_rated), abs(e_walk[step - 1] / tstep))
                e_walk[step] = e_walk[step - 1] - sto_dispatch[step] * tstep  # kWh
            else:
                sto_dispatch[step] = 0
                e_walk[step] = e_walk[step - 1]
        kwh_min = max(e_walk) - min(e_walk)
        self.min_power_requirement = float(kw_rated)
        self.min_energy_requirement = float(kwh_min)
        return [self.min_power_requirement, self.min_energy_requirement]

    def set_last_deferral_year(self, last_year):
        """Sets last year that deferral is possible

        Args:
            last_year (int): The last year
        """
        self.last_year = last_year
