"""
FrequencyRegulation.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import cvxpy as cvx
import pandas as pd
import numpy as np
import Library as Lib
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class FrequencyRegulation(ValueStream):
    """ Frequency Regulation. Each service will be daughters of the ValueStream class.

    """

    def __init__(self, params, tech, dt):
        """ Generates the objective function, finds and creates constraints.
        Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            dt (float): optimization timestep (hours)

        To Do:
            - determine a method to provide values for the min/max values of k-values if min/max are not provided
        """
        # financials_df = financials.fin_inputs
        ValueStream.__init__(self, tech, 'FR', dt)
        # self.fr_energyprice = params['energyprice']
        self.krd_avg = params['kd']
        self.krd_min = 0  # figure out how to handle these
        self.krd_max = 0
        self.kru_avg = params['ku']
        self.kru_min = 0
        self.kru_max = 0
        self.combined_market = params['CombinedMarket']  # boolean: true if storage bid as much reg up as reg down
        self.price = params['energy_price']
        self.p_regu = params['regu_price']
        self.p_regd = params['regd_price']
        self.growth = params['growth']
        self.energy_growth = params['energyprice_growth']

    def objective_function(self, opt_vars, subs):
        """ Generates the full objective function, including the optimization variables. Saves generated expression to
        within the class.

        Args:
            opt_vars (Dict): dictionary of variables being optimized
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            The portion of the objective function that it affects. This can be passed into the cvxpy solver.

        """

        # pay for reg down energy, get paid for reg up energy
        # paid revenue for capacity to do both

        p_regu = cvx.Parameter(subs.index.size, value=self.p_regu.loc[subs.index].values, name='p_regu')
        p_regd = cvx.Parameter(subs.index.size, value=self.p_regd.loc[subs.index].values, name='p_regd')
        p_ene = cvx.Parameter(subs.index.size, value=self.price.loc[subs.index].values, name='price')

        regu_c_cap = cvx.sum(-opt_vars['regu_c'] * p_regu)
        regu_c_ene = cvx.sum(-opt_vars['regu_c'] * self.kru_avg * p_ene * self.dt)

        regu_d_cap = cvx.sum(-opt_vars['regu_d'] * p_regu)
        regu_d_ene = cvx.sum(-opt_vars['regu_d'] * self.kru_avg * p_ene * self.dt)

        regd_c_cap = cvx.sum(-opt_vars['regd_c'] * p_regd)
        regd_c_ene = cvx.sum(opt_vars['regd_c'] * self.krd_avg * p_ene * self.dt)

        regd_d_cap = cvx.sum(-opt_vars['regd_d'] * p_regd)
        regd_d_ene = cvx.sum(opt_vars['regd_d'] * self.krd_avg * p_ene * self.dt)

        self.costs.append(regu_c_cap + regu_c_ene + regu_d_cap + regu_d_ene + regd_c_cap + regd_c_ene + regd_d_cap + regd_d_ene)
        return {'regu_c_cap': regu_c_cap, 'regu_c_ene': regu_c_ene,
                'regu_d_cap': regu_d_cap, 'regu_d_ene': regu_d_ene,
                'regd_c_cap': regd_c_cap, 'regd_c_ene': regd_c_ene,
                'regd_d_cap': regd_d_cap, 'regd_d_ene': regd_d_ene}

    @staticmethod
    def add_vars(size):
        """ Adds optimization variables to dictionary

        Variables added:
            regu_c (Variable): A cvxpy variable for freq regulation capacity to increase charging power
            regd_c (Variable): A cvxpy variable for freq regulation capacity to decrease charging power
            regu_d (Variable): A cvxpy variable for freq regulation capacity to increase discharging power
            regd_d (Variable): A cvxpy variable for freq regulation capacity to decrease discharging power

        Args:
            size (Int): Length of optimization variables to create

        Returns:
            Dictionary of optimization variables
        """
        return {'regu_c': cvx.Variable(shape=size, name='regu_c'),
                'regd_c': cvx.Variable(shape=size, name='regd_c'),
                'regu_d': cvx.Variable(shape=size, name='regu_d'),
                'regd_d': cvx.Variable(shape=size, name='regd_d')}

    def build_constraints(self, opt_vars, subs):
        """Default build constraint list method. Used by services that do not have constraints.

        Args:
            opt_vars (Dict): dictionary of variables being optimized
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            constraint_list (list): list of constraints

        """
        constraint_list = []
        constraint_list += [cvx.NonPos(-opt_vars['regu_c'])]
        constraint_list += [cvx.NonPos(-opt_vars['regd_c'])]
        constraint_list += [cvx.NonPos(-opt_vars['regu_d'])]
        constraint_list += [cvx.NonPos(-opt_vars['regd_d'])]
        # p = opt_vars['dis'] - opt_vars['ch']
        # constraint_list += [cvx.NonPos(opt_vars['regd_d'] - cvx.pos(p))]
        # constraint_list += [cvx.NonPos(opt_vars['regu_c'] - cvx.neg(p))]
        if self.combined_market:
            constraint_list += [cvx.Zero(opt_vars['regd_d'] + opt_vars['regd_c'] - opt_vars['regu_d'] - opt_vars['regu_c'])]

        return constraint_list

    def power_ene_reservations(self, opt_vars):
        """ Determines power and energy reservations required at the end of each timestep for the service to be provided.
        Additionally keeps track of the reservations per optimization window so the values maybe accessed later.

        Args:
            opt_vars (Dict): dictionary of variables being optimized

        Returns:
            A power reservation and a energy reservation array for the optimization window--
            C_max, C_min, D_max, D_min, E_upper, E, and E_lower (in that order)
        """
        eta = self.storage.rte

        # calculate reservations
        c_max = opt_vars['regd_c']
        c_min = opt_vars['regu_c']
        d_min = opt_vars['regd_d']
        d_max = opt_vars['regu_d']

        e_upper = self.krd_max * self.dt * opt_vars['regd_d'] + self.krd_max * self.dt * opt_vars['regd_c'] * eta - self.kru_min * self.dt * opt_vars['regu_d'] - self.kru_min * self.dt * opt_vars[
                      'regu_c'] * eta
        e = self.krd_avg*self.dt*opt_vars['regd_d'] + self.krd_avg*self.dt*opt_vars['regd_c']*eta - self.kru_avg*self.dt*opt_vars['regu_d'] - self.kru_avg*self.dt*opt_vars['regu_c']*eta
        e_lower = self.krd_min*self.dt*opt_vars['regd_d'] + self.krd_min*self.dt*opt_vars['regd_c']*eta - self.kru_max*self.dt*opt_vars['regu_d'] - self.kru_max*self.dt*opt_vars['regu_c']*eta

        # save reservation for optmization window
        self.e.append(e)
        self.e_lower.append(e_lower)
        self.e_upper.append(e_upper)
        self.c_max.append(c_max)
        self.c_min.append(c_min)
        self.d_max.append(d_max)
        self.d_min.append(d_min)
        return [c_max, c_min, d_max, d_min], [e_upper, e, e_lower]

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        data_year = self.price.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                source_data = self.price[self.price.index.year == source_year.year]  # use source year data
                new_data = Lib.apply_growth(source_data, self.energy_growth, source_year, yr, frequency)
                self.price = pd.concat([self.price, new_data], sort=True)  # add to existing

                source_data = self.p_regu[self.p_regu.index.year == source_year.year]  # use source year data
                new_data = Lib.apply_growth(source_data, self.growth, source_year, yr, frequency)
                self.p_regu = pd.concat([self.p_regu, new_data], sort=True)  # add to existing

                source_data = self.p_regd[self.p_regd.index.year == source_year.year]  # use source year data
                new_data = Lib.apply_growth(source_data, self.growth, source_year, yr, frequency)
                self.p_regd = pd.concat([self.p_regd, new_data], sort=True)  # add to existing
