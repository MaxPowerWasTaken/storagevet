"""
DemandResponse.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import pandas as pd
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class DemandResponse(ValueStream):
    """ Demand response program participation. Each service will be daughters of the ValueStream class.

    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)

            TODO: this code was originally written for a load_data and fin_data DataFrames and has not been editted to work
        """

        # generate the generic service object
        ValueStream.__init__(self, tech, 'Deferral', dt)

        # add dr specific attributes to object
        self.dr_days = params['days']
        self.dr_length = params['length']
        self.dr_time = params['time']
        self.dr_energy_reservation = params['energy_reservation']
        self.dr_weekend = params['weekend']
        self.dr_program_start_hour = params['program_start_hour']
        self.dr_program_end_hour = params['program_end_hour']
        self.dt = params['dt']
        self.system_load = params['system_load']
        self.cap_price = params['cap_price']
        self.ene_price = params['ene_price']
        self.months = params['dr_months']
        self.cap = params['dr_cap']

        # # merge load and financial data
        # time_series = pd.merge(pd.DataFrame(load_data[['yr_mo', 'opt_agg', 'date', 'weekday', 'he', 'system_load', 'dr_months', 'dr_cap']]),
        #                        pd.DataFrame(fin_data[['yr_mo', 'opt_agg', 'date',  'weekday', 'he', 'dr_cap_price', 'dr_ene_price']]),
        #                        on=['yr_mo', 'opt_agg', 'weekday', 'date',  'he'], left_index=True, right_index=True)

        index = self.system_load.index
        he = (index + pd.Timedelta('1s')).hour + 1
        day = index.to_period('D')
        # dr program is active based on month and if hour is in program hours
        self.active = (self.months == 1) & (he > self.dr_program_start_hour) & (he <= self.dr_program_end_hour)

        # remove weekends from active datetimes if dr_weekends is False
        SATURDAY = 5
        weekday = (index.weekday < SATURDAY).astype('int64')
        if not self.dr_weekend:
            self.active.loc[weekday == 0, :] = 0  # TODO: check to make sure this

        # TODO exclude weekends??
        # find dr_days number of days where system_load is at  during eligible periods
        max_days = self.system_load[self.months == 1].groupby(by=day).max()
        self.disp_days = max_days[0:self.dr_days].index.values

        # # create dispatch power constraint
        # time_series['dis_min'] = 0
        # time_series['dr_ene'] = 0

        # TODO fix for sub hour dispatch
        # index of DR datetimes
        temp_dttm = pd.Timestamp("01/01/2018 "+self.dr_time.__str__())
        start_hour = temp_dttm.hour  # hour beginning
        end_hour = (temp_dttm + pd.Timedelta(self.dr_length, unit="h")).hour  # hour ending
        indx = (day.isin(self.disp_days)) & (he > start_hour) & (he <= end_hour)

        # time_series.loc[indx, 'dis_min'] = self.cap.loc[indx, :]  # tech must discharge to meet dispatch capacity
        # time_series.loc[indx, 'dr_ene'] = self.cap.loc[indx, :]*self.dt  # energy discharged during time period
        # dis_min_add = Const.Constraint('dis_min_add', self.name, time_series['dis_min'])
        # self.constraints = {'dis_min_add': dis_min_add}
        # # Note: currently DR requires battery discharge. Could make it an option to reduce charging instead to meet commitment?
        #
        # if self.dr_energy_reservation:
        #     # create energy reservation constraint for all active dr hours
        #     time_series['ene_min'] = 0
        #     time_series.loc[self.active == 1, 'ene_min'] = self.cap[self.active == 1]*self.dr_length
        #     ene_min_add = Const.Constraint('ene_min_add', self.name, time_series['ene_min'])
        #     self.constraints.update({'ene_min_add': ene_min_add})

        # # add financials
        # time_series['dr_cap_pay'] = time_series['dr_cap'] * time_series['dr_cap_price']
        # time_series = lib.disagg_col(time_series, ['yr_mo', 'opt_agg'], 'dr_cap_pay')
        # time_series['dr_ene_pay'] = time_series['dr_ene'] * time_series['dr_ene_price']
        #
        # self.obj_val = -time_series.groupby('opt_agg')['dr_cap_pay_disagg', 'dr_ene_pay'].sum()
        # self.mean_inputs = time_series[['opt_agg', 'dr_months', 'dr_cap', 'dr_cap_price', 'dr_ene_price']].groupby('opt_agg').mean()
