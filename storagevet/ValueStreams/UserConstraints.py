"""
UserConstraints.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import Constraint as Const
from ValueStreams.ValueStream import ValueStream
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class UserConstraints(ValueStream):
    """ User entered time series constraints. Each service will be daughters of the PreDispService class.

    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)
        """
        # generate the generic service object
        ValueStream.__init__(self, tech, 'UserConstraints', dt)

        self.user_constraints = params['constraints']
        user_cols = list(self.user_constraints)
        self.constraints = {}
        for col in user_cols:
            strp = col.split('_')
            if strp[1] == "min":
                new_col = col + "_add"
                new_col_val = self.user_constraints[col]
            else:
                new_col = col + "_sub"
                new_col_val = self.storage.physical_constraints[col+'_rated'].value - self.user_constraints[col]

            setattr(self, new_col, new_col_val)  #TODO: do not use this!!
            self.constraints[new_col] = Const.Constraint(new_col, self.name, new_col_val)
