"""
EnergyTimeShift.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import numpy as np
import cvxpy as cvx
import pandas as pd
import Library as Lib
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class EnergyTimeShift(ValueStream):
    """ Retail energy time shift. A behind the meter service.

    """

    def __init__(self, params, tech, dt):
        """ Generates the objective function, finds and creates constraints.

        Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            dt (float): optimization timestep (hours)
        """
        ValueStream.__init__(self, tech, 'retailETS', dt)
        self.p_energy = params['price']
        self.tariff = params['tariff']

    def objective_function(self, variables, subs):
        """ Generates the full objective function, including the optimization variables.

        Args:
            variables (Dict): Dictionary of optimization variables
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            The expression of the objective function that it affects. This can be passed into the cvxpy solver.

        """
        size = subs.index.size
        load = cvx.Parameter(size, value=np.array(subs.loc[:, "load"]), name='load')
        gen = cvx.Parameter(size, value=np.array(subs.loc[:, "generation"]), name='gen')
        p_energy = cvx.Parameter(size, value=self.p_energy.loc[subs.index].values, name='energy_price')
        self.costs.append(cvx.sum(p_energy * load * self.dt - p_energy * variables['dis'] * self.dt + p_energy * variables['ch'] * self.dt - p_energy * gen * self.dt))
        return {self.name: cvx.sum(p_energy*load*self.dt - p_energy*variables['dis']*self.dt + p_energy*variables['ch']*self.dt - p_energy*gen*self.dt)}

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        SATURDAY = 5
        data_year = self.p_energy.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                source_data = self.p_energy[self.p_energy.index.year == source_year.year]  # use source year data

                first_day = source_data.index[0].date() + pd.DateOffset(years=1)
                first_day = pd.Period(freq='D', day=first_day.day, month=first_day.month, year=yr.year)

                last_day = source_data.index[-1].date() + pd.Timedelta('1 day')
                last_day = pd.Period(freq='D', day=last_day.day, month=last_day.month, year=yr.year + 1)

                new_index = pd.date_range(start=first_day.to_timestamp(), end=last_day.to_timestamp(), freq=frequency, closed='left')
                size = source_data.index.size
                # Build Energy Price Vector
                temp = pd.DataFrame(index=new_index)
                temp['weekday'] = (new_index.weekday < SATURDAY).astype('int64')
                temp['he'] = (new_index + pd.Timedelta('1s')).hour + 1
                temp['hour fraction'] = temp.index.minute / 60
                temp['he fraction'] = temp['he'] + temp['hour fraction']

                new_p_energy = pd.DataFrame({'p_energy': np.zeros(size)}, index=new_index)

                for p in range(len(self.tariff)):
                    # edit the pricedf energy price and period values for all of the periods defined
                    # in the tariff input file
                    bill = self.tariff.iloc[p, :]

                    month_mask = (bill["Start Month"] <= temp.index.month) & (temp.index.month <= bill["End Month"])
                    time_mask = (bill['Start Time'] <= temp['he fraction']) & (temp['he fraction'] <= bill['End Time'])
                    weekday_mask = True
                    exclud_mask = False

                    if not bill['Weekday?'] == 2:  # if not (apply to weekends and weekdays)
                        weekday_mask = bill['Weekday?'] == temp['weekday']
                    if not np.isnan(bill['Excluding Start Time']) and not np.isnan(bill['Excluding End Time']):
                        exclud_mask = (bill['Excluding Start Time'] <= temp['he fraction']) & (temp['he fraction'] <= bill['Excluding End Time'])
                    mask = np.array(month_mask & time_mask & np.logical_not(exclud_mask) & weekday_mask)

                    current_energy_prices = temp.loc[mask, 'p_energy'].values
                    if np.any(np.greater(current_energy_prices, 0)):
                        # More than one energy price applies to the same time step
                        uLogger.warning('More than one energy price applies to the same time step.')
                    # Add energy prices
                    temp.loc[mask, 'p_energy'] += bill['Value']
                self.p_energy = pd.concat([self.p_energy, new_p_energy], sort=True)  # add to existing

