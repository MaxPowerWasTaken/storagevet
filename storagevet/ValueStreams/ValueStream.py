"""
ValueStreamream.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import os
import sys
import numpy as np
import cvxpy as cvx
import Library as sh
import pandas as pd
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class ValueStream:
    """ A general template for services provided and constrained by the providing technologies.

    """

    def __init__(self, storage, name, dt):
        """ Initialize all services with the following attributes

        Args:
            storage (Technology): Storage technology object
            name (str): A string name/description for the service
            dt (float): optimization timestep (hours)
        """
        self.name = name

        # power and energy requirements
        self.c_max = []
        self.c_min = []
        self.d_min = []
        self.d_max = []
        self.e_upper = []
        self.e = []
        self.e_lower = []

        self.constraints = {}  # TODO: rename this (used in calculate_control_constraints)

        self.storage = storage
        # TODO: might be cleaner to pass in technologies when there is more than 1 (instead of saving as attributes

        # this will be minimized in the optimization problem
        self.costs = []

        self.dt = dt
        self.ene_results = pd.DataFrame()

    def objective_function(self, variables, subs):
        """ Generates the full objective function, including the optimization variables.

        Args:
            variables (Dict): dictionary of variables being optimized
            subs (DataFrame): table of load data for the optimization windows

        Returns:
            The portion of the objective function that it affects. Default is to return 0.
        """
        return 0

    def build_constraints(self, opt_vars, subs):
        """Default build constraint list method. Used by services that do not have constraints.

        Args:
            opt_vars (Dict): dictionary of variables being optimized
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            An empty list
        """
        return []

    def power_ene_reservations(self, opt_vars):
        """ Determines power and energy reservations required at the end of each timestep for the service to be provided.
        Additionally keeps track of the reservations per optimization window so the values maybe accessed later.

        Args:
            opt_vars (Dict): dictionary of variables being optimized

        Returns:
            A power reservation and a energy reservation array for the optimization window--
            C_max, C_min, D_max, D_min, E_upper, E, and E_lower (in that order)
        """
        size = opt_vars['ene'].shape
        # calculate reservations
        c_max = 0
        c_min = 0
        d_min = 0
        d_max = 0
        e_upper = cvx.Parameter(shape=size, value=np.zeros(size), name='e_upper')
        e = cvx.Parameter(shape=size, value=np.zeros(size), name='e')
        e_lower = cvx.Parameter(shape=size, value=np.zeros(size), name='e_lower')

        # save reservation for optmization window
        self.e.append(e)
        self.e_lower.append(e_lower)
        self.e_upper.append(e_upper)
        self.c_max.append(c_max)
        self.c_min.append(c_min)
        self.d_max.append(d_max)
        self.d_min.append(d_min)
        return [c_max, c_min, d_max, d_min], [e_upper, e, e_lower]

    def __eq__(self, other, compare_init=False):
        """ Determines whether ValueStreams object equals another ValueStreams object. Compare_init = True will do an
        initial comparison ignoring any attributes that are changed in the course of running a case.

        Args:
            other (ValueStream): ValueStreams object to compare
            compare_init (bool): Flag to ignore attributes that change after initialization

        Returns:
            bool: True if objects are close to equal, False if not equal.
        """
        return sh.compare_class(self, other, compare_init)

    @staticmethod
    def add_vars(size):
        """ Default method that will not create any new optimization variables

        Args:
            size (int): length of optimization variables to create

        Returns:
            An empty set
        """
        return {}

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        pass
















