"""
SpinningReserve.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import numpy as np
import cvxpy as cvx
import Library as sh
import pandas as pd
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class SpinningReserve(ValueStream):
    """ Spinning Reserve. Each service will be daughters of the ValueStream class.

    """

    def __init__(self, params, tech, dt):
        """ Generates the objective function, finds and creates constraints.

        Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            dt (float): optimization timestep (hours)
        """
        ValueStream.__init__(self, tech, 'SR', dt)
        self.price = params['price']
        self.growth = params['growth']  # growth rate of spinning reserve price (%/yr)

    @staticmethod
    def add_vars(size):
        """ Adds optimization variables to dictionary

        Variables added:
            sr_c (Variable): A cvxpy variable for spinning reserve capacity to increase charging power
            sr_d (Variable): A cvxpy variable for spinning reserve capacity to decrease charging power

        Args:
            size (Int): Length of optimization variables to create

        Returns:
            Dictionary of optimization variables
        """
        return {'sr_c': cvx.Variable(shape=size, name='sr_c'),
                'sr_d': cvx.Variable(shape=size, name='sr_d')}

    def objective_function(self, opt_vars, subs):
        p_sr = cvx.Parameter(subs.index.size, value=self.price.loc[subs.index].values, name='price')

        self.costs.append(-p_sr * opt_vars['sr_c'] - p_sr * opt_vars['sr_d'])
        return {self.name: -p_sr*opt_vars['sr_c']*self.dt - p_sr*opt_vars['sr_d']*self.dt}

    def build_constraints(self, opt_vars, subs):
        """Default build constraint list method. Used by services that do not have constraints.

        Args:
            opt_vars (Dict): dictionary of variables being optimized
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:

        """
        constraint_list = []
        constraint_list += [0 <= opt_vars['sr_c']]
        constraint_list += [0 <= opt_vars['sr_d']]
        return constraint_list

    def power_ene_reservations(self, opt_vars):
        """ Determines power and energy required at the end of each timestep for the service to be provided.
        Additionally keeps track of the reservations per optimization window so the values maybe accessed later.

        Args:
            opt_vars (Dict): dictionary of variables being optimized

        Returns:
            A power reservation and a energy reservation array for the optimization window--
            C_max, C_min, D_max, D_min, E_upper, E, and E_lower (in that order)

        TODO: might be cleaner to pass in technologies when there is more than 1 (instead of saving as attributs
        """
        eta = self.storage.rte
        size = opt_vars['ene'].shape
        # calculate reservations
        c_max = 0
        c_min = opt_vars['sr_c']
        d_min = 0
        d_max = opt_vars['sr_d']
        e_upper = cvx.Parameter(shape=size, value=np.zeros(size), name='e_upper')
        e = cvx.Parameter(shape=size, value=np.zeros(size), name='e')
        e_lower = -opt_vars['sr_c']*eta*self.dt - opt_vars['sr_d']*self.dt

        # save reservation for optimization window
        self.e.append(e)
        self.e_lower.append(e_lower)
        self.e_upper.append(e_upper)
        self.c_max.append(c_max)
        self.c_min.append(c_min)
        self.d_max.append(d_max)
        self.d_min.append(d_min)
        return [c_max, c_min, d_max, d_min], [e_upper, e, e_lower]

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        data_year = self.price.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                source_data = self.price[self.price.index.year == source_year.year]  # use source year data
                new_data = sh.apply_growth(source_data, self.growth, source_year, yr, frequency)
                self.price = pd.concat([self.price, new_data], sort=True)  # add to existing
