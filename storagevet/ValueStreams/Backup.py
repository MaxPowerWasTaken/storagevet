"""
Backup.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import Constraint as Const
import Library as sh
import pandas as pd
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class Backup(ValueStream):
    """ Backup Energy. Each service will be daughters of the PreDispService class.

    # TODO backup energy should not stack???
    TODO: TEST !!!!!!!!!!!!!!!!!!
    """

    def __init__(self, params, tech, load_data, dt):
        """ Generates the objective function, finds and creates constraints.

          Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            load_data (DataFrame): table of time series load data
            dt (float): optimization timestep (hours)
        """

        # generate the generic service object
        ValueStream.__init__(self, tech, 'Deferral', dt)
        self.energy_req = params['energy']
        self.price = params['price']

        # merge load and fin data
        time_series = pd.merge(self.energy_req, self.price)
        # backup energy adds a minimum energy level
        ene_min_add = Const.Constraint('ene_min_add', self.name, self.energy_req)
        self.constraints = {'ene_min_add': ene_min_add}
        # self.mean_inputs = time_series[['opt_agg', 'backup_energy', 'backup_price']].groupby('opt_agg').mean()  #TODO: what does this do? commented out for now --HN

        # add financials
        # assumes that opt_agg is less or equal to month
        time_series = sh.disagg_col(time_series, ['yr_mo', 'opt_agg'], 'backup_price')
        self.obj_val = -time_series.groupby('opt_agg')['backup_price_disagg'].sum()

    def estimate_year_data(self, years, frequency):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            years (List): list of years for which analysis will occur on
            frequency (str): period frequency of the timeseries data

        """
        data_year = self.energy_req.index.year.unique()
        no_data_year = {pd.Period(year) for year in years} - {pd.Period(year) for year in data_year}  # which years do we not have data for

        if len(no_data_year) > 0:
            for yr in no_data_year:
                source_year = pd.Period(max(data_year))

                # create new dataframe for missing year
                new_index = pd.date_range(start='01/01/' + str(yr), end='01/01/' + str(yr + 1), freq=frequency, closed='left')
                new_data = pd.DataFrame(index=new_index)

                source_data = self.energy_req[self.energy_req.index.year == source_year.year]  # use source year data
                # growth rate is 0 because source_data is originally given monthly
                new_data = sh.apply_growth(source_data, 0, source_year, yr, frequency)
                self.energy_req = pd.concat([self.energy_req, new_data], sort=True)  # add to existing
        # backup energy adds a minimum energy level
        ene_min_add = Const.Constraint('ene_min_add', self.name, self.energy_req)
        self.constraints = {'ene_min_add': ene_min_add}
