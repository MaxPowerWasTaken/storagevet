"""
DemandChargeReduction.py

This Python class contains methods and attributes specific for service analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from ValueStreams.ValueStream import ValueStream
import numpy as np
import cvxpy as cvx
import pandas as pd
import sys
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')

class DemandChargeReduction(ValueStream):
    """ Retail demand charge reduction. A behind the meter service.

    """

    def __init__(self, params, tech, dt):
        """ Generates the objective function, finds and creates constraints.

        Args:
            params (Dict): input parameters
            tech (Technology): Storage technology object
            dt (float): optimization timestep (hours)
        """
        ValueStream.__init__(self, tech, 'DCM', dt)
        # self.demand_rate = params['rate']
        self.tariff = params['tariff']
        self.billing_period = params['billing_period']

    def objective_function(self, variables, subs):
        """ Generates the full objective function, including the optimization variables.

        Args:
            variables (Dict): Dictionary of optimization variables
            subs (DataFrame): Subset of time_series data that is being optimized

        Returns:
            An Expression--The portion of the objective function that it affects. This can be passed into the cvxpy solver.

        """
        # pandas converts the billing period lists to ints if there are only one
        # per time step. This checks for that and handles appropriately
        sub_billing_period = self.billing_period.loc[subs.index]

        demand_charge = 0
        generation_tot = np.array(subs.loc[:, "generation"])
        load_array = np.array(subs.loc[:, "load"])
        net_load = load_array - variables['dis'] + variables['ch'] - generation_tot

        # if mask contains more than a month, then add dcterm monthly
        yr_mo = sub_billing_period.index.to_period('M')
        pset = {int(item) for sublist in sub_billing_period for item in sublist}
        for year_month in yr_mo.unique():
            for per in pset:  # Add demand charge calculation for each applicable billing period
                billing_per_mask = []
                for i in range(sub_billing_period.size):
                    tru_false = per in sub_billing_period.iloc[i] and sub_billing_period.index[i].month == year_month.month and sub_billing_period.index[i].year == year_month.year
                    billing_per_mask.append(tru_false)
                demand_charge += self.tariff.loc[per, 'Value'] * cvx.max(net_load[billing_per_mask])
        self.costs.append(demand_charge)
        return {self.name: demand_charge}

    def estimate_year_data(self, time_series, fin_inputs):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            time_series (DataFrame): the mutated time_series data with extrapolated load data
            fin_inputs (DataFrame): the mutated time_series data with extrapolated price data

        """
        # self.billing_period = fin_inputs.loc[:, 'billing_period']
        pass
