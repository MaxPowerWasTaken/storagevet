"""
runStorageVET.py

This Python script serves as the initial launch point executing the Python-based version of StorageVET
(AKA StorageVET 2.0 or SVETpy).
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani',
               'Micah Botkin-Levy', "Thien Nguyen", 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import argparse
import Scenario
from pathlib import Path
from Params import Params
from Result import Result

import logging
from pathlib import Path
import time
from datetime import datetime
import os
import sys

# TODO: make multi-platform by using path combine functions
# TODO: set working directory path to be where the function is called (for results folder)

developer_path = Path('./logs')
try:
    os.mkdir(developer_path)
except OSError:
    print("Creation of the developer_log directory %s failed. Possibly already created." % developer_path)
else:
    print("Successfully created the developer_log directory %s " % developer_path)

LOG_FILENAME1 = developer_path.joinpath(f"developer_log_{datetime.now().strftime('%H_%M_%S_%m_%d_%Y.log')}")
handler = logging.FileHandler(Path(LOG_FILENAME1))
handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
dLogger = logging.getLogger('Developer')
dLogger.setLevel(logging.DEBUG)
dLogger.addHandler(handler)
dLogger.info('Started logging...')

TEMPLATE_HEADERS = ['Key', 'Units', 'Description', 'Options']


class run_StorageVET:

    def __init__(self, params):
        """
            Constructor to initialize the parameters and data needed to run StorageVET\

            Args:
                params (Params.Params): an initialized Params object for the class to initialize
        """

        self.p = params

        starts = time.time()

        for key, value in self.p.instances.items():
            if not value.other_error_checks():
                continue
            value.prepare_scenario()
            value.prepare_technology()
            value.prepare_services()
            value.prepare_finance()

            run = Scenario.Scenario(value)
            run.add_technology()
            run.add_services()
            run.add_control_constraints()
            run.optimize_problem_loop()

            results = Result(run)
            results.post_analysis()
            results.save_results_csv()

        ends = time.time()
        dLogger.info("runStorageVET runtime: ")
        dLogger.info(ends - starts)


if __name__ == '__main__':
    """
        the Main section for runStorageVET to run by itself without the GUI 
    """

    parser = argparse.ArgumentParser(prog='StorageVET.py',
                                     description='The Electric Power Research Institute\'s energy storage system ' +
                                                 'analysis, dispatch, modelling, optimization, and valuation tool' +
                                                 '. Should be used with Python 3.6.x, pandas 0.19+.x, and CVXPY' +
                                                 ' 0.4.x or 1.0.x.',
                                     epilog='Copyright 2018. Electric Power Research Institute (EPRI). ' +
                                            'All Rights Reserved.')
    parser.add_argument('parameters_filename', type=str,
                        help='specify the filename of the CSV file defining the PARAMETERS dataframe')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='specify this flag for verbose output during execution')
    parser.add_argument('--gitlab-ci', action='store_true',
                        help='specify this flag for gitlab-ci testing to skip user input')
    arguments = parser.parse_args()

    dLogger.info('Finished basic configuration with the provided file: %s', arguments.parameters_filename)

    if arguments.parameters_filename.endswith(".csv"):
        arguments.parameters_filename = Params.csv_to_xml(arguments.parameters_filename)

    # Initialize the Params Object from Model Parameters and Simulation Cases
    script__rel_path = sys.argv[0]
    dir_rel_path = script__rel_path[:-len('run_StorageVET.py')]
    schema_rel_path = dir_rel_path + "Schema.xml"

    Params.initialize(arguments.parameters_filename, schema_rel_path)
    dLogger.info('Successfully initialized the Params class with the XML file.')

    active_commands = Params.active_components['command']
    if "Results_Directory" in active_commands:
        userLog_path = Path(Params.csv_path)
        try:
            os.makedirs(userLog_path)
        except OSError:
            print("Creation of the user_log directory %s failed. Possibly already created." % userLog_path)
        else:
            print("Successfully created the user_log directory %s " % userLog_path)
    else:
        userLog_path = developer_path

    LOG_FILENAME2 = userLog_path.joinpath(f"user_log_{datetime.now().strftime('%H_%M_%S_%m_%d_%Y.log')}")

    handler = logging.FileHandler(Path(LOG_FILENAME2))
    handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    uLogger = logging.getLogger('User')
    uLogger.setLevel(logging.DEBUG)
    uLogger.addHandler(handler)
    uLogger.info('Started logging...')

    if not arguments.gitlab_ci:
        if "Previsualization" in active_commands:
            Params.class_summary()
            uLogger.info('Successfully ran the pre-visualization.')

        if "Validation" in active_commands:
            Params.validate()
            uLogger.info('Successfully ran validate.')

        if "Simulation" in active_commands:
            run_StorageVET(Params)
            uLogger.info('Simulation ran successfully.')
    else:
        Params.class_summary()
        Params.validate()
        run_StorageVET(Params)
        dLogger.info('Simulation ran pre-visualization, validate, and successfully finished.')

    print("Program is done.")
