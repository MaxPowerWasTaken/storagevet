"""
Library.py

Library of helper functions used in StorageVET.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import Technology as Tech
import ValueStreams as Srv
import numpy as np
import pandas as pd
import copy

BOUND_NAMES = ['ch_max', 'ch_min', 'dis_max', 'dis_min', 'ene_max', 'ene_min']
# bound_names = ['ch_max', 'ch_min', 'dis_max', 'dis_min', 'ene_max', 'ene_min']
fr_obj_names = ['regu_d_cap', 'regu_c_cap', 'regd_d_cap', 'regd_c_cap', 'regu_d_ene', 'regu_c_ene', 'regd_d_ene', 'regd_c_ene']


def update_df(df1, df2):
    """ Helper function: Updates elements of df1 based on df2. Will add new columns if not in df1 or insert elements at
    the corresponding index if existing column

    Args:
        df1 (Data Frame): original data frame to be editted
        df2 (Data Frame): data frame to be added

    Returns:
        df1 (Data Frame)
    """

    old_col = set(df2.columns).intersection(set(df1.columns))
    df1 = df1.join(df2[list(set(df2.columns).difference(old_col))], how='left')  # join new columns
    df1.update(df2[list(old_col)])  # update old columns
    return df1


def disagg_col(df, group, col):
    """ Helper function: Adds a disaggregated column of 'col' based on the count of group
    TEMP FUNCTION: assumes that column is merged into disagg dataframe at original resolution = Bad approach

    Args:
        df (Data Frame): original data frame to be
        group (list): columns to group on
        col (string): column to disagg

    Returns:
        df (Data Frame)

    Notes:
        It is not clear from the description above what this function does... -HN

    """
    # TODO this is a temp helper function until a more robust disagg function is built

    count_df = df.groupby(by=group).size()
    count_df.name = 'counts'
    df = df.reset_index().merge(count_df.reset_index(), on=group, how='left').set_index(df.index.names)
    df[col+'_disagg'] = df[col] / df['counts']
    return df


def create_opt_agg(df, n, dt):
    """ Helper function: Add opt_agg column to df based on n

    Args:
        df (Data Frame)
        n (): optimization control length
        dt (float): time step

    Returns:
        df (Data Frame)
    """
    # optimization level
    prev = 0
    df['opt_agg'] = 0
    # opt_agg period should not overlap multiple years
    for yr in df.index.year.unique():
        sub = copy.deepcopy(df[df.index.year == yr])
        if n == 'year':
            df.loc[df.index.year == yr, 'opt_agg'] = prev + 1  # continue counting from previous year opt_agg
        elif n == 'month':
            df.loc[df.index.year == yr, 'opt_agg'] = prev + sub.index.month  # continue counting from previous year opt_agg
        elif n == 'mpc':
            n = 1
            sub = copy.deepcopy(df[df.index.year == yr])
            sub['ind'] = range(len(sub))
            ind = (sub.ind // (n/dt)).astype(int) + 1
            df.loc[df.index.year == yr, 'opt_agg'] = ind + prev  # continue counting from previous year opt_agg
        else:  # assume n number of hours
            n = int(n)
            sub = copy.deepcopy(df[df.index.year == yr])
            sub['ind'] = range(len(sub))
            ind = (sub.ind // (n/dt)).astype(int)+1  # split year into groups of n days
            df.loc[df.index.year == yr, 'opt_agg'] = ind + prev  # continue counting from previous year opt_agg
        prev = max(df.opt_agg)

    return df


def apply_growth(source, rate, source_year, yr, freq):
    """ Applies linear growth rate to determine data for future year

    Args:
        source (Series): given data
        rate (float): yearly growth rate (%)
        source_year (Period): given data year
        yr (Period): future year to get data for
        freq (str): simulation time step frequency

    Returns:
        new (Series)
    """
    years = yr.year - source_year.year  # difference in years between source and desired yea
    if rate:
        new = source*(1+rate/100)**years  # apply growth rate to source data
    else:
        new = source
    # new.index = new.index + pd.DateOffset(years=1)
    # deal with leap years
    source_leap = is_leap_yr(source_year.year)
    new_leap = is_leap_yr(yr.year)

    if (not source_leap) and new_leap:   # need to add leap day
        # if source is not leap year but desired year is, copy data from previous day
        new.index = new.index + pd.DateOffset(years=1)
        leap_ind = pd.date_range(start='02/29/'+str(yr), end='03/01/'+str(yr), freq=freq, closed='left')
        leap = pd.Series(new[leap_ind - pd.DateOffset(days=1)].values, index=leap_ind, name=new.name)
        new = pd.concat([new, leap])
        new = new.sort_index()
    elif source_leap and (not new_leap):  # need to remove leap day
        leap_ind = pd.date_range(start='02/29/'+str(source_year), end='03/01/'+str(source_year), freq=freq, closed='left')
        new = new[~new.index.isin(leap_ind)]
        new.index = new.index + pd.DateOffset(years=1)
    else:
        new.index = new.index + pd.DateOffset(years=1)
    return new


# def remove_year(series, year)


def compare_class(c1, c2, compare_init=False, debug_flag=False):
    """ Determines whether class object equals another class object.
    Compare_init = True will do an initial comparison ignoring any attributes that are changed in the course of running a case.

    Args:
        c1 : Class object to compare
        c2 : Class object to compare
        compare_init (bool): Flag to ignore attributes that change after initialization. Default False
        debug_flag (bool): Flag to print or return not equal attributes. Default False


    Returns:
        bool: True if objects are close to equal, False if not equal. (only if debug_flag is False)
    """

    if c1.__class__ == c2.__class__:

        # all attributes from both classes
        attributes = list(set().union(c1.__dict__, c2.__dict__))

        # attributes that should not be compared
        always_ignore = ['inputs', 'results_path', 'verbose', 'verbose_opt', 'start_time', 'end_time']  # from case
        attributes = np.setdiff1d(attributes, always_ignore)

        # attributes that should not be compared if the class has only been initialized
        ignore_init_fields = ['power_kw', 'monthly_bill', 'technologies', 'services', 'predispatch_services', 'financials', 'results']  # from case
        ignore_init_fields += ['degrade_data', 'predispatch_services', 'services', 'costs', 'physical_constraints', 'control_constraints']  # from tech
        ignore_init_fields += ['costs', 'technologies', 'c_max', 'c_min', 'd_max', 'd_min', 'e', 'e_lower', 'e_upper', 'ene_results']  # from service
        ignore_init_fields += ['fin_summary', 'monthly_bill', 'obj_val']  # from financials
        ignore_init_fields += ['c_max', 'c_min', 'd_max', 'd_min', 'e', 'e_lower', 'e_upper']  # from services

        if compare_init:
            attributes = np.setdiff1d(attributes, ignore_init_fields)

        # for each attribute compare from class 1 and class 2 if possible
        for attr in attributes:
            print(('class attr:' + attr)) if debug_flag else None
            attr1 = None
            attr2 = None

            try:
                attr1 = getattr(c1, attr)
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False
            try:
                attr2 = getattr(c2, attr)
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False

            if (attr1 is not None) & (attr2 is not None):
                compare_attribute(attr1, attr2, parent_class=c1, debug_flag=debug_flag)

        return True
    else:
        print('Class types not equal') if debug_flag else None
        return False


def compare_attribute(attr1, attr2, parent_class=None, debug_flag=False):
    """ Determines whether class attribute equals another class attribute.

    Args:
        attr1 : Attribute object to compare
        attr2 : Attribute object to compare
        debug_flag (bool): Flag to print or return not equal attributes

    Returns:
        bool: True if objects are close to equal, False if not equal. (only if debug_flag is False)
    """

    import cvxpy as cvx

    # determines attribute type based on first attribute passed
    if isinstance(attr1, pd.core.frame.DataFrame):

        # loop over each column and compare if column is in both data frames
        for n in list(set().union(attr1, attr2)):
            print(('dataframe col:' + n)) if debug_flag else None
            col1 = None
            col2 = None
            try:
                col1 = attr1[n]
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False
            try:
                col2 = attr2[n]
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False
            if (col1 is not None) & (col2 is not None):
                compare_attribute(col1, col2, debug_flag=debug_flag)

        # # this is faster but cant do percent diff
        # test = pd.testing.assert_frame_equal(attr1, attr2,
        #                                      check_less_precise=4, check_like=True)
    elif isinstance(attr1, (np.ndarray, pd.core.series.Series, pd.core.indexes.datetimes.DatetimeIndex)):

        # just need for fin_results billing_period right now
        if attr1.dtype == object:
            if not attr1.equals(attr2):
                if debug_flag:
                    print('false')
                else:
                    return False
        else:
            compare_array(attr1, attr2, debug_flag=debug_flag)
    elif isinstance(attr1, list):
        for attr1a, attr2a in zip(list(attr1), list(attr2)):
            # print(('list item:' + str(item))) if debug_flag else None
            compare_attribute(attr1a, attr2a, debug_flag=debug_flag)  # recursive call here to loop over elements in list
    elif isinstance(attr1, dict):
        for item in list(attr1):
            # print(('dict item:' + str(item))) if debug_flag else None
            item1 = None
            item2 = None
            try:
                item1 = attr1[item]
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False
            try:
                item2 = attr2[item]
            except (AttributeError, KeyError):
                if debug_flag:
                    print('false')
                else:
                    return False
            if (item1 is not None) & (item2 is not None):
                compare_attribute(item1, item2, parent_class=parent_class, debug_flag=debug_flag)  # recursive call here to loop over elements in dict
    elif isinstance(attr1, cvx.expressions.expression.Expression):
        if not attr1.__str__() == attr2.__str__():  # compare cvxpy expression as string
            if debug_flag:
                print('false')
            else:
                return False

    # this important to prevent infinite recursion (i.e srv -> tech ->srv)
    elif (isinstance(attr1, Tech.Storage)) & (isinstance(parent_class, (Srv.PreDispService, Srv.ValueStream))):
        if debug_flag:
            print('skipping tech under service')
    else:
        if not attr1 == attr2:  # this could recall compare_class if a custom class
            if debug_flag:
                print('false')
            else:
                return False


def compare_array(arr1, arr2, rtol=1e-4, debug_flag=False):
    """ Determines whether class attribute equals another class attribute.

    Args:
        arr1 : Array to compare
        arr2 : Array to compare
        rtol: relative tolerance to compare
        debug_flag (bool): Flag to print or return not equal attributes

    Returns:
        bool: True if objects are close to equal, False if not equal. (only if debug_flag is False)
    """

    # relative tolerance is same as percent difference
    if not np.allclose(arr1, arr2, rtol=rtol, equal_nan=True):
        if debug_flag:
            print('false')
        else:
            return False


def is_leap_yr(year):
    """ Determines whether given year is leap year or not.

    Args:
        year (int): The year in question.

    Returns:
        bool: True for it being a leap year, False if not leap year.
    """
    return year % 4 == 0 and year % 100 != 0 or year % 400 == 0
