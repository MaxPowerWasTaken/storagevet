"""
BatteryTech.py

This Python class contains methods and attributes specific for technology analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from Technology.Storage import Storage
import copy, logging
import numpy as np
import pandas as pd
import rainflow

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class BatteryTech(Storage):
    """ Battery class that inherits from Storage.

    """

    def __init__(self, name,  opt_agg, params, tech_params, cycle_life):
        """ Initializes a battery class that inherits from the technology class.
        It sets the type and physical constraints of the technology.

        Args:
            name (string): name of technology
            opt_agg (DataFrame): Initalized Financial Class
            params (dict): params dictionary from dataframe for one case
            cycle_life (DataFrame): Cycle life information
        """

        # create generic storage object
        Storage.__init__(self, name, tech_params)

        # add degradation information
        self.cycle_life = cycle_life
        self.degrade_data = pd.DataFrame(index=opt_agg.unique())

        # calculate current degrade_perc since installation
        if self.incl_cycle_degrade:
            start_dttm = opt_agg.index[0].to_timestamp()
            self.calc_degradation(None, self.install_date, start_dttm)
            self.degrade_data['degrade_perc'] = self.degrade_perc
            self.degrade_data['eff_e_cap'] = self.apply_degradation()

    def build_master_constraints(self, variables, mask, reservations, mpc_ene=None):
        """ Builds the master constraint list for the subset of timeseries data being optimized.

        Args:
            variables (Dict): Dictionary of variables being optimized
            mask (DataFrame): A boolean array that is true for indices corresponding to time_series data included
                in the subs data set
            reservations (Dict): Dictionary of energy and power reservations required by the services being
                preformed with the current optimization subset
            mpc_ene (float): value of energy at end of last opt step (for mpc opt)

        Returns:
            A list of constraints that corresponds the battery's physical constraints and its
            service constraints.
        """

        # create default list of constraints
        constraint_list = Storage.build_master_constraints(self, variables, mask, reservations, mpc_ene)
        # add constraint that battery can not charge and discharge in the same timestep
        if self.incl_binary:
            # can only be on or off
            constraint_list += [variables['on_c'] + variables['on_d'] <= 1]

            # # when trying non binary
            # constraint_list += [0 <= on_c]
            # constraint_list += [0 <= on_d]

            # # NL formulation of binary variables
            # constraint_list += [cvx.square(on_c) - on_c == 0]
            # constraint_list += [cvx.square(on_d) - on_d == 0]

        return constraint_list

    def calc_degradation(self, opt_period, start_dttm, end_dttm, energy_series=None):
        """ calculate degradation percent based on yearly degradation and cycle degradation

        Args:
            start_dttm (DateTime): Start timestamp to calculate degradation
            end_dttm (DateTime): End timestamp to calculate degradation
            energy_series (Series): time series of energy values

        Returns:
            A percent that represented the energy capacity degradation
        """

        # time difference between time stamps converted into years multiplied by yearly degrate rate
        # TODO dont hard code 365 (leap year)
        if self.incl_cycle_degrade:
            time_degrade = min((end_dttm - start_dttm).days/365*self.yearly_degrade/100, 1)

            # if given energy data and user wants cycle degradation
            if energy_series is not None:
                # use rainflow counting algorithm to get cycle counts
                cycle_counts = rainflow.count_cycles(energy_series, ndigits=4)

                # sort cycle counts into user inputed cycle life bins
                digitized_cycles = np.searchsorted(self.cycle_life['Cycle Depth Upper Limit'],
                                                   [min(i[0]/self.ene_max_rated, 1) for i in cycle_counts], side='left')

                # sum up number of cycles for all cycle counts in each bin
                cycle_sum = copy.deepcopy(self.cycle_life)
                cycle_sum['cycles'] = 0
                for i in range(len(cycle_counts)):
                    cycle_sum.loc[digitized_cycles[i], 'cycles'] += cycle_counts[i][1]

                # sum across bins to get total degrade percent
                # 1/cycle life value is degrade percent for each cycle
                cycle_degrade = np.dot(1/cycle_sum['Cycle Life Value'], cycle_sum.cycles)
            else:
                cycle_degrade = 0

            degrade_percent = time_degrade + cycle_degrade
            if opt_period:
                self.degrade_data.loc[opt_period, 'degrade_perc'] = degrade_percent + self.degrade_perc
            else:
                self.degrade_perc = degrade_percent + self.degrade_perc

    def apply_degradation(self, datetimes=None):
        """ Updates ene_max_rated and control constraints based on degradation percent

        Args:
            datetimes (DateTime): Vector of timestamp to recalculate control_constraints. Default is None which results in control constraints not updated

        Returns:
            Degraded energy capacity
        """

        # apply degrade percent to rated energy capacity
        new_ene_max = max(self.ulsoc*self.ene_max_rated*(1-self.degrade_perc), 0)

        # update physical constraint
        self.physical_constraints['ene_max_rated'].value = new_ene_max

        failure = None
        if datetimes is not None:
            # update control constraints
            failure = self.calculate_control_constraints(datetimes)
        if failure is not None:
            # possible that degredation caused infeasible scenario
            dLogger.error('Degradation results in infeasible scenario')
            quit()
        return new_ene_max
