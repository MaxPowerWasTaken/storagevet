"""
CAESTech.py

This Python class contains methods and attributes specific for technology analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

from Technology.Storage import Storage
import cvxpy as cvx
import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class CAESTech(Storage):
    """ CAES class that inherits from Storage.

    """

    def __init__(self, name,  opt_agg, params, tech_params, cycle_life):
        """ Initializes a battery class that inherits from the technology class.
        It sets the type and physical constraints of the technology.

        Args:
            name (string): name of technology
            financial (Analysis): Initalized Financial Class
            params (dict): params dictionary from dataframe for one case
        """

        # create generic technology object  TODO: name and category are the same...did we do this on purpose?
        Storage.__init__(self, name, tech_params)

        # add CAES specific attributes
        self.heat_rate_high = params['heat_rate_high']
        self.fuel_price = params['fuel_price']  # $/MillionBTU

    def objective_function(self, variables, mask):
        """ Generates the objective costs for fuel cost and O&M cost

         Args:
            variables (Dict): dictionary of variables being optimized
            mask (Series): Series of booleans used, the same length as case.power_kw

        Returns:
            self.costs (Dict): Dict of objective costs

        """
        # get generic Tech objective costs
        Storage.objective_function(self, variables, mask)

        # add fuel cost expression
        fuel_exp = cvx.sum(cvx.multiply(self.fuel_price[mask].values, variables['dis'])*self.heat_rate_high*self.dt*1e-6)
        self.expressions.update({'CAES_fuel_cost': fuel_exp})

        return self.expressions

    def calc_operating_cost(self, energy_rate, fuel_rate):
        """ Calculates operating cost in dollars per MWh_out

         Args:
            energy_rate (float): energy rate [=] $/kWh
            fuel_rate (float): fuel rate [=] $/MillionBTU

        Returns:
            Value of Operating Cost [=] $/MWh_out

        """
        fuel_cost = fuel_rate*self.heat_rate_high*1e3/1e6
        om = self.fixedOM
        energy = energy_rate*1e3/self.rte

        return fuel_cost + om + energy