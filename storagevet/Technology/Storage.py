"""
Storage

This Python class contains methods and attributes specific for technology analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import copy, logging
import Constraint as Const
import cvxpy as cvx
import numpy as np
import pandas as pd
import re
import Library as Lib
import sys
from Technology.DER import DER

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class Storage(DER):
    """ A general template for storage object

    We define "storage" as anything that can affect the quantity of load/power being delivered or used. Specific
    types of storage are subclasses. The storage subclass should be called. The storage class should never
    be called directly.

    """

    def __init__(self, name, params):
        """ Initialize all technology with the following attributes.

        Args:
            name (str): A unique string name for the technology being added, also works as category.
            params (dict): Dict of parameters
        """
        # create generic technology object
        DER.__init__(self, name)
        # input params
        # note: these should never be changed in simulation (i.e from degradation)
        self.name = name
        self.install_date = params['install_date']
        self.rte = params['rte']/100
        self.sdr = params['sdr']
        self.ene_max_rated = params['ene_max_rated']
        self.dis_max_rated = params['dis_max_rated']
        self.dis_min_rated = params['dis_min_rated']
        self.ch_max_rated = params['ch_max_rated']
        self.ch_min_rated = params['ch_min_rated']
        self.ulsoc = params['ulsoc']/100
        self.llsoc = params['llsoc']/100
        self.soc_target = params['soc_target']/100
        self.yearly_degrade = params['yearly_degrade']
        self.incl_cycle_degrade = bool(params['incl_cycle_degrade'])  # this is a true / false -EG
        self.ccost = params['ccost']
        self.ccost_kw = params['ccost_kw']
        self.ccost_kwh = params['ccost_kwh']
        self.fixedOM = params['fixedOM']
        self.OMexpenses = params['OMexpenses']
        self.incl_startup = params['startup']
        self.incl_slack = params['slack']
        self.incl_binary = params['binary']
        self.dt = params['dt']
        self.degrade_perc = 0
        if self.incl_startup:
            self.p_start_ch = params['p_start_ch']
            self.p_start_dis = params['p_start_dis']
        if self.incl_slack:
            self.kappa_ene_max = params['kappa_ene_max']
            self.kappa_ene_min = params['kappa_ene_min']
            self.kappa_ch_max = params['kappa_ch_max']
            self.kappa_ch_min = params['kappa_ch_min']
            self.kappa_dis_max = params['kappa_dis_max']
            self.kappa_dis_min = params['kappa_dis_min']

        # create physical constraints from input parameters
        # note: these can be changed throughout simulation (i.e. from degradation)
        self.physical_constraints = {'ene_min_rated': Const.Constraint('ene_min_rated', self.name, self.llsoc*self.ene_max_rated),
                                     'ene_max_rated': Const.Constraint('ene_max_rated', self.name, self.ulsoc*self.ene_max_rated),
                                     'ch_min_rated': Const.Constraint('ch_min_rated', self.name, self.ch_min_rated),
                                     'ch_max_rated': Const.Constraint('ch_max_rated', self.name, self.ch_max_rated),
                                     'dis_min_rated': Const.Constraint('dis_min_rated', self.name, self.dis_min_rated),
                                     'dis_max_rated': Const.Constraint('dis_max_rated', self.name, self.dis_max_rated)}

        self.degrade_data = None

    def objective_function(self, variables, mask):
        """ Generates the objective function related to a technology. Default includes O&M which can be 0

        Args:
            variables (Dict): dictionary of variables being optimized
            mask (Series): Series of booleans used, the same length as case.power_kw

        Returns:
            self.costs (Dict): Dict of objective costs
        """

        # time difference over optimization window
        time_diff = (mask[mask].index[-1]+pd.Timedelta(self.dt, unit='h')) - mask[mask].index[0]

        # TODO dont hard code 365 (leap year)
        # create constant objective expression for fixed_om
        fixed_om = self.fixedOM*time_diff.days/365*self.physical_constraints['dis_max_rated'].value
        fixed_om = cvx.Constant(fixed_om)  # uses the CXVpy type and allows .value attribute
        # create objective expression for variable om based on discharge activity
        var_om = cvx.sum(variables['dis']*self.OMexpenses*self.dt*1e-3)

        self.expressions = {'fixed_om': fixed_om,
                            'var_om': var_om}

        # add slack objective costs. These try to keep the slack variables as close to 0 as possible
        if self.incl_slack:
            self.expressions.update({
                'ene_max_slack': cvx.sum(self.kappa_ene_max * variables['ene_max_slack']),
                'ene_min_slack': cvx.sum(self.kappa_ene_min * variables['ene_min_slack']),
                'dis_max_slack': cvx.sum(self.kappa_dis_max * variables['dis_max_slack']),
                'dis_min_slack': cvx.sum(self.kappa_dis_min * variables['dis_min_slack']),
                'ch_max_slack': cvx.sum(self.kappa_ch_max * variables['ch_max_slack']),
                'ch_min_slack': cvx.sum(self.kappa_ch_min * variables['ch_min_slack'])})

        # add startup objective costs
        if self.incl_startup:
            self.expressions.update({
                          'ch_startup': cvx.sum(variables['start_c']*self.p_start_ch),
                          'dis_startup': cvx.sum(variables['start_d']*self.p_start_dis)})

        return self.expressions

    def build_master_constraints(self, variables, mask, reservations, mpc_ene=None):
        """ Builds the master constraint list for the subset of timeseries data being optimized.

        Args:
            variables (Dict): Dictionary of variables being optimized
            mask (DataFrame): A boolean array that is true for indices corresponding to time_series data included
                in the subs data set
            reservations (Dict): Dictionary of energy and power reservations required by the services being
                preformed with the current optimization subset
            mpc_ene (float): value of energy at end of last opt step (for mpc opt)

        Returns:
            A list of constraints that corresponds the battery's physical constraints and its service constraints
        """

        constraint_list = []

        size = int(np.sum(mask))

        curr_e_cap = self.physical_constraints['ene_max_rated'].value
        ene_target = self.soc_target * curr_e_cap

        # optimization variables
        ene = variables['ene']
        dis = variables['dis']
        ch = variables['ch']
        on_c = variables['on_c']
        on_d = variables['on_d']

        # create cvx parameters of control constraints (this improves readability in cvx costs and better handling)
        ene_max = cvx.Parameter(size, value=self.control_constraints['ene_max'].value[mask].values, name='ene_max')
        ene_min = cvx.Parameter(size, value=self.control_constraints['ene_min'].value[mask].values, name='ene_min')
        ch_max = cvx.Parameter(size, value=self.control_constraints['ch_max'].value[mask].values, name='ch_max')
        ch_min = cvx.Parameter(size, value=self.control_constraints['ch_min'].value[mask].values, name='ch_min')
        dis_max = cvx.Parameter(size, value=self.control_constraints['dis_max'].value[mask].values, name='dis_max')
        dis_min = cvx.Parameter(size, value=self.control_constraints['dis_min'].value[mask].values, name='dis_min')

        # energy at the end of the last time step
        constraint_list += [cvx.Zero((ene_target - ene[-1]) - (self.dt * ch[-1] * self.rte) + (self.dt * dis[-1]) - reservations['E'][-1] + (self.dt * ene[-1] * self.sdr * 0.01))]

        # energy generally for every time step
        constraint_list += [cvx.Zero(ene[1:] - ene[:-1] - (self.dt * ch[:-1] * self.rte) + (self.dt * dis[:-1]) - reservations['E'][:-1] + (self.dt * ene[:-1] * self.sdr * 0.01))]

        # energy at the beginning of the optimization window
        if mpc_ene is None:
            constraint_list += [cvx.Zero(ene[0] - ene_target)]
        else:
            constraint_list += [cvx.Zero(ene[0] - mpc_ene)]

        # # Keep energy in bounds determined in the constraints configuration function
        constraint_list += [cvx.NonPos(ene_target - ene_max[-1] + reservations['E_upper'][-1] - variables['ene_max_slack'][-1])]
        constraint_list += [cvx.NonPos(ene[1:] - ene_max[1:] + reservations['E_upper'][:-1] - variables['ene_max_slack'][:-1])]

        constraint_list += [cvx.NonPos(-ene_target + ene_min[-1] - reservations['E_lower'][-1] - variables['ene_min_slack'][-1])]
        constraint_list += [cvx.NonPos(ene_min[1:] - ene[1:] + reservations['E_lower'][:-1] - variables['ene_min_slack'][:-1])]

        # Keep charge and discharge power levels within bounds
        constraint_list += [cvx.NonPos(ch - cvx.multiply(ch_max, on_c) - variables['ch_max_slack'])]
        constraint_list += [cvx.NonPos(ch - ch_max + reservations['C_max'] - variables['ch_max_slack'])]

        constraint_list += [cvx.NonPos(cvx.multiply(ch_min, on_c) - ch - variables['ch_min_slack'])]
        constraint_list += [cvx.NonPos(ch_min - ch + reservations['C_min'] - variables['ch_min_slack'])]

        constraint_list += [cvx.NonPos(dis - cvx.multiply(dis_max, on_d) - variables['dis_max_slack'])]
        constraint_list += [cvx.NonPos(dis - dis_max + reservations['D_max'] - variables['dis_max_slack'])]

        constraint_list += [cvx.NonPos(cvx.multiply(dis_min, on_d) - dis - variables['dis_min_slack'])]
        constraint_list += [cvx.NonPos(dis_min - dis + reservations['D_min'] - variables['dis_min_slack'])]
        # constraints to keep slack variables positive
        if self.incl_slack:
            constraint_list += [cvx.NonPos(-variables['ch_max_slack'])]
            constraint_list += [cvx.NonPos(-variables['ch_min_slack'])]
            constraint_list += [cvx.NonPos(-variables['dis_max_slack'])]
            constraint_list += [cvx.NonPos(-variables['dis_min_slack'])]
            constraint_list += [cvx.NonPos(-variables['ene_max_slack'])]
            constraint_list += [cvx.NonPos(-variables['ene_min_slack'])]

        if self.incl_binary:
            # when dis_min or ch_min has been overwritten (read: increased) by predispatch services, need to force technology to be on
            # TODO better way to do this???
            ind_d = [i for i in range(size) if self.control_constraints['dis_min'].value[mask].values[i] > self.physical_constraints['dis_min_rated'].value]
            ind_c = [i for i in range(size) if self.control_constraints['ch_min'].value[mask].values[i] > self.physical_constraints['ch_min_rated'].value]
            if len(ind_d) > 0:
                constraint_list += [on_d[ind_d] == 1]  # np.ones(len(ind_d))
            if len(ind_c) > 0:
                constraint_list += [on_c[ind_c] == 1]  # np.ones(len(ind_c))

            # note: cannot operate startup without binary
            if self.incl_startup:
                # startup variables are positive
                constraint_list += [cvx.NonPos(-variables['start_d'])]
                constraint_list += [cvx.NonPos(-variables['start_c'])]
                # difference between binary variables determine if started up in previous interval
                constraint_list += [cvx.NonPos(cvx.diff(on_d) - variables['start_d'][1:])]  # first variable not constrained
                constraint_list += [cvx.NonPos(cvx.diff(on_c) - variables['start_c'][1:])]  # first variable not constrained

        return constraint_list

    def calculate_control_constraints(self, datetimes):
        """ Generates a list of master or 'control constraints' from physical constraints and all
        predispatch service constraints.

        Args:
            datetimes (list): The values of the datetime column within the initial time_series data frame.

        Returns:
            Array of datetimes where the control constraints conflict and are infeasible. If all feasible return None.

        Note: the returned failed array returns the first infeasibility found, not all feasibilities.
        TODO: come back and check the user inputted constraints --HN
        """
        # create temp dataframe with values from physical_constraints
        temp_constraints = pd.DataFrame(index=datetimes)
        # create a df with all physical constraint values
        for constraint in self.physical_constraints.values():
            temp_constraints[re.search('^.+_.+_', constraint.name).group(0)[0:-1]] = copy.deepcopy(constraint.value)

        # change physical constraint with predispatch service constraints at each timestep
        # predispatch service constraints can add to minimums or subtract from maximums (formulated this way so they can stack)
        for service in self.predispatch_services.values():
            for constraint in service.constraints.values():
                if constraint.value is not None:
                    strp = constraint.name.split('_')
                    const_name = strp[0]
                    const_type = strp[1]
                    name = const_name + '_' + const_type

                    if const_type == "min":
                        # if minimum constraint, add predispatch constraint value
                        temp_constraints.loc[constraint.value.index, name] += constraint.value.values

                        # if the minimum value needed is greater than the physical maximum, infeasible scenario
                        max_value = self.physical_constraints[const_name + '_max' + '_rated'].value
                        if any(temp_constraints[name] > max_value):
                            return temp_constraints[temp_constraints[name] > max_value].index

                    else:
                        # if maximum constraint, subtract predispatch constraint value
                        min_value = self.physical_constraints[const_name + '_min' + '_rated'].value
                        temp_constraints.loc[constraint.value.index, name] -= constraint.value.values

                        if (const_name == 'ene') & any(temp_constraints[name] < min_value):
                            # if the maximum energy needed is less than the physical minimum, infeasible scenario
                            return temp_constraints[temp_constraints[name] > max_value].index
                        else:
                            # it is ok to floor at zero since negative power max values will be handled in power min
                            # i.e negative ch_max means dis_min should be positive and ch_max should be 0)
                            temp_constraints[name] = temp_constraints[name].clip(lower=0)
            if service.name == 'UserConstraints':
                user_inputted_constraint = service.user_constraints
                for user_constraint_name in user_inputted_constraint:
                    # determine if the user inputted constraint is a max or min constraint
                    user_constraint = user_inputted_constraint[user_constraint_name]
                    const_type = user_constraint_name.split('_')[1]
                    if const_type == 'max':
                        # iterate through user inputted constraint Series
                        for i in user_constraint.index:
                            # update temp_constraints df if user inputted a lower max constraint
                            if temp_constraints[user_constraint_name].loc[i] > user_constraint.loc[i]:
                                temp_constraints[user_constraint_name].loc[i] = user_constraint.loc[i]
                    elif const_type == 'min':
                        # iterate through user inputted constraint Series
                        for i in user_constraint.index:
                            # update temp_constraints df if user inputted a higher min constraint
                            if temp_constraints[user_constraint_name].loc[i] < user_constraint.loc[i]:
                                temp_constraints[user_constraint_name].loc[i] = user_constraint.loc[i]
                    else:
                        dLogger.error("User has inputted an invalid constraint for Storage. Please change and run again.")

                        sys.exit()

        # now that we have a new list of constraints, create Constraint objects and store as 'control constraint'
        self.control_constraints = {'ene_min': Const.Constraint('ene_min', self.name, temp_constraints['ene_min']),
                                    'ene_max': Const.Constraint('ene_max', self.name, temp_constraints['ene_max']),
                                    'ch_min': Const.Constraint('ch_min', self.name, temp_constraints['ch_min']),
                                    'ch_max': Const.Constraint('ch_max', self.name, temp_constraints['ch_max']),
                                    'dis_min': Const.Constraint('dis_min', self.name, temp_constraints['dis_min']),
                                    'dis_max': Const.Constraint('dis_max', self.name, temp_constraints['dis_max'])}
        return None

    def calc_degradation(self, opt_period, start_dttm, end_dttm, energy_series=None):
        """ Default is zero degradation
        Args:
            start_dttm (DateTime): Start timestamp to calculate degradation
            end_dttm (DateTime): End timestamp to calculate degradation
            energy_series (Series): time series of energy values

        Returns:
            A percent that represented the energy capacity degradation
        """
        return 0

    def apply_degradation(self, datetimes):
        """ Default is no degradation effect

        Args:
            datetimes (DateTime): Vector of timestamp to recalculate control_constraints

        Returns:
            Degraded energy capacity
        """
        pass

    def add_vars(self, size):
        """ Adds optimization variables to dictionary

        Variables added:
            ene (Variable): A cvxpy variable for Energy at the end of the time step
            dis (Variable): A cvxpy variable for Discharge Power, kW during the previous time step
            ch (Variable): A cvxpy variable for Charge Power, kW during the previous time step
            ene_max_slack (Variable): A cvxpy variable for energy max slack
            ene_min_slack (Variable): A cvxpy variable for energy min slack
            ch_max_slack (Variable): A cvxpy variable for charging max slack
            ch_min_slack (Variable): A cvxpy variable for charging min slack
            dis_max_slack (Variable): A cvxpy variable for discharging max slack
            dis_min_slack (Variable): A cvxpy variable for discharging min slack

        Args:
            size (Int): Length of optimization variables to create

        Returns:
            Dictionary of optimization variables
        """

        variables = {'ene': cvx.Variable(shape=size, name='ene'),
                     'dis': cvx.Variable(shape=size, name='dis'),
                     'ch': cvx.Variable(shape=size, name='ch'),
                     'ene_max_slack': cvx.Parameter(shape=size, name='ene_max_slack', value=np.zeros(size)),
                     'ene_min_slack': cvx.Parameter(shape=size, name='ene_min_slack', value=np.zeros(size)),
                     'dis_max_slack': cvx.Parameter(shape=size, name='dis_max_slack', value=np.zeros(size)),
                     'dis_min_slack': cvx.Parameter(shape=size, name='dis_min_slack', value=np.zeros(size)),
                     'ch_max_slack': cvx.Parameter(shape=size, name='ch_max_slack', value=np.zeros(size)),
                     'ch_min_slack': cvx.Parameter(shape=size, name='ch_min_slack', value=np.zeros(size)),
                     'on_c': cvx.Parameter(shape=size, name='on_c', value=np.ones(size)),
                     'on_d': cvx.Parameter(shape=size, name='on_d', value=np.ones(size)),
                     }

        if self.incl_slack:
            variables.update({'ene_max_slack': cvx.Variable(shape=size, name='ene_max_slack'),
                              'ene_min_slack': cvx.Variable(shape=size, name='ene_min_slack'),
                              'dis_max_slack': cvx.Variable(shape=size, name='dis_max_slack'),
                              'dis_min_slack': cvx.Variable(shape=size, name='dis_min_slack'),
                              'ch_max_slack': cvx.Variable(shape=size, name='ch_max_slack'),
                              'ch_min_slack': cvx.Variable(shape=size, name='ch_min_slack')})
        if self.incl_binary:
            variables.update({'on_c': cvx.Variable(shape=size, boolean=True, name='on_c'),
                              'on_d': cvx.Variable(shape=size, boolean=True, name='on_d')})
            if self.incl_startup:
                variables.update({'start_c': cvx.Variable(shape=size, name='start_c'),
                                  'start_d': cvx.Variable(shape=size, name='start_d')})

        return variables

    def __eq__(self, other, compare_init=False):
        """ Determines whether Storage object equals another Storage object. Compare_init = True will do an
        initial comparison ignoring any attributes that are changed in the course of running a case.

        Args:
            other (Storage): Storage object to compare
            compare_init (bool): Flag to ignore attributes that change after initialization

        Returns:
            bool: True if objects are close to equal, False if not equal.
        """
        return Lib.compare_class(self, other, compare_init)

    def apply_past_degredation(self, ind, power_kw, mask, opt_period, n):
        """

        Args:
            ind (int):
            power_kw (DataFrame): dataframe of system generation and load with opt_agg column
            mask (DataFrame): A boolean array that is true for indices corresponding to time_series data included
                in the subs data set
            opt_period (int):
            n (str/int): optimization window size

        """
        periods = pd.Series(copy.deepcopy(power_kw.opt_agg.unique()))
        if self.degrade_data is not None and self.incl_cycle_degrade:
            # if time period since end of last optimization period is greater than dt need to estimate missing degradation
            # time_diff = (self.power_kw[mask].index[0]-self.power_kw[self.power_kw.opt_agg == periods[max(ind - 1, 0)]].index[-1])
            time_diff = 0
            if ind:
                prev_opt_period_time_end = power_kw[power_kw.opt_agg == periods[ind - 1]].index[-1].to_timestamp()
                cur_opt_period_time_start = power_kw[mask].index[0].to_timestamp()
                time_diff = (cur_opt_period_time_start - prev_opt_period_time_end)
            if time_diff > pd.Timedelta(self.dt, unit='h'):
                avg_degrade = np.mean(np.diff(self.degrade_data.degrade_perc[0:ind]))
                days_opt_agg = 30 if n == 'month' else int(n)
                self.degrade_perc = avg_degrade / days_opt_agg * time_diff.days  # this could be a better estimation
            else:
                # else look back to previous degrade rate
                self.degrade_perc = self.degrade_data.iloc[max(ind - 1, 0)].loc['degrade_perc']

            # apply degradation to technology (affects physical_constraints['ene_max_rated'] and control constraints)
            self.degrade_data.loc[opt_period, 'eff_e_cap'] = self.apply_degradation(power_kw.index)
