"""
Generator

This Python class contains methods and attributes specific for technology analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']


class Generator:
    """ A general template for generator object

    """

    def __init__(self, name, params):
        """ Initialize all technology with the following attributes.

        Args:
            name (str): A unique string name for the technology being added, also works as category.
            params (dict): Dict of parameters for initialization
        """

        # input params  UNITS ARE COMMENTED TO THE RIGHT
        self.name = name
        self.p_max = params['rated_power']  # kW
        self.p_min = params['min_power']  # kW
        self.startup = params['startup_time']   # minutes
        self.efficiency = params['efficiency']  # gal/kWh
        self.fuel_cost = params['fuel_cost']  # $/gal
        self.vari_om = params['variable_om_cost']  # $/kwh
        self.fixed_om = params['fixed_om_cost']  # $/yr
        self.marcs = params['marcs_term']  # years
        self.capital_cost = params['ccost']  # $
