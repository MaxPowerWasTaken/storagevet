"""
Technology

This Python class contains methods and attributes specific for technology analysis within StorageVet.
"""

__author__ = 'Miles Evans and Evan Giarta'
__copyright__ = 'Copyright 2018. Electric Power Research Institute (EPRI). All Rights Reserved.'
__credits__ = ['Miles Evans', 'Andres Cortes', 'Evan Giarta', 'Halley Nathwani', 'Micah Botkin-Levy', 'Yekta Yazar']
__license__ = 'EPRI'
__maintainer__ = ['Evan Giarta', 'Miles Evans']
__email__ = ['egiarta@epri.com', 'mevans@epri.com']

import logging

dLogger = logging.getLogger('Developer')
uLogger = logging.getLogger('User')


class DER:
    """ A general template for Technology object

    We define a "Technology" as anything that might have an effect on the power into or out of the system
    in question.

    """

    def __init__(self, name):
        """ Initialize all technology with the following attributes.

        Args:
            name (str): A unique string name for the technology being added, also works as category.
        """

        # initialize internal attributes
        self.control_constraints = {}
        self.services = {}
        self.predispatch_services = {}
        self.type = name
        self.expressions = {}

        # not currently used
        self.load = 0
        self.generation = 0

    def add_value_streams(self, service, predispatch=False):
        """ Adds a service to the list of services provided by the technology.

        Args:
            service (:obj, ValueStream): A ValueStream class object
            predispatch (Boolean): Flag to add predispatch or dispatch service
        """
        if predispatch:
            self.predispatch_services[service.name] = service
        else:
            self.services[service.name] = service

    def objective_function(self, variables, mask):
        """ Generates the objective function related to a technology. Default includes O&M which can be 0

        Args:
            variables (Dict): dictionary of variables being optimized
            mask (Series): Series of booleans used, the same length as case.power_kw

        Returns:
            self.costs (Dict): Dict of objective costs
        """
        return self.expressions

    def build_master_constraints(self, variables, mask, reservations, mpc_ene=None):
        """ Builds the master constraint list for the subset of timeseries data being optimized.

        Args:
            variables (Dict): Dictionary of variables being optimized
            mask (DataFrame): A boolean array that is true for indices corresponding to time_series data included
                in the subs data set
            reservations (Dict): Dictionary of energy and power reservations required by the services being
                preformed with the current optimization subset
            mpc_ene (float): value of energy at end of last opt step (for mpc opt)

        Returns:
            A list of constraints that corresponds the battery's physical constraints and its service constraints
        """

        constraint_list = []
        return constraint_list

    def calculate_control_constraints(self, datetimes):
        """ Generates a list of master or 'control constraints' from physical constraints and all
        predispatch service constraints.

        Args:
            datetimes (list): The values of the datetime column within the initial time_series data frame.

        Returns:
            Array of datetimes where the control constraints conflict and are infeasible. If all feasible return None.

        Note: the returned failed array returns the first infeasibility found, not all feasibilities.
        """
        return None

    def add_vars(self, size):
        """ Adds optimization variables to dictionary

        Variables added:
            ene (Variable): A cvxpy variable for Energy at the end of the time step
            dis (Variable): A cvxpy variable for Discharge Power, kW during the previous time step
            ch (Variable): A cvxpy variable for Charge Power, kW during the previous time step
            ene_max_slack (Variable): A cvxpy variable for energy max slack
            ene_min_slack (Variable): A cvxpy variable for energy min slack
            ch_max_slack (Variable): A cvxpy variable for charging max slack
            ch_min_slack (Variable): A cvxpy variable for charging min slack
            dis_max_slack (Variable): A cvxpy variable for discharging max slack
            dis_min_slack (Variable): A cvxpy variable for discharging min slack

        Args:
            size (Int): Length of optimization variables to create

        Returns:
            Dictionary of optimization variables
        """

        variables = {}

        return variables

    def update_data(self, fin_inputs):
        """ Update variable that hold timeseries data after adding growth data. These method should be called after
        add_growth_data and before the optimization is run.

        Args:
            fin_inputs (DataFrame): the mutated time_series data with extrapolated price data

        """
        pass

