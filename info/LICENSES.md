The SVETpy repository and source distributions bundle several libraries that are
compatibly licensed.  We list these here.

Name: numpy  
Files:  
License: BSD-style

Name: pandas  
Files:  
License: 3-Clause BSD

Name: matplotlib  
Files:  
License: PSF (BSD-style)

Name: cvxpy  
Files:  
License:  Apache 2.0 and GNU GPL  

Name: plotly
Files:
License: MIT

Name: cvxopt
Files:
License: GNU General Public License

Name: ecos
Files:
License: GNU General Public License

Name: scipy 
Files:
License: 3-clause BSD

Name: rainflow
Files:
License: Other/Proprietary License 

Name: pyperclip
Files:
License: BSD-style

Name: prettytable
Files:
License: BSD-style